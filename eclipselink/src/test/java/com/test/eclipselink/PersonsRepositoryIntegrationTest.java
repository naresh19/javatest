package com.test.eclipselink;

import com.test.eclipselink.model.Person;
import com.test.eclipselink.repo.PersonsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

@SpringBootTest
public class PersonsRepositoryIntegrationTest {

	@Autowired
	private PersonsRepository personsRepository;

	@Test
	public void givenPerson_whenSave_thenAddOnePersonToDB() {
		Person person1 = new Person();
		person1.setFirstName("Adam1");
		person1.setLastName("Adam1");
		personsRepository.save(person1);
		assertThat(personsRepository.findAll().spliterator().getExactSizeIfKnown(), equalTo(1l));
	}

	@Test
	public void givenPersons_whenSearch_thenFindOk() {
		Person person1 = new Person();
		person1.setFirstName("Adam");
		person1.setLastName("Adam");

		Person person2 = new Person();
		person2.setFirstName("Dave");
		person2.setLastName("Dave");

		personsRepository.save(person1);
		personsRepository.save(person2);

		Person foundPerson = personsRepository.findByFirstName("Adam");

		assertThat(foundPerson.getFirstName(), equalTo("Adam"));
		assertThat(foundPerson.getId(), notNullValue());
	}

}