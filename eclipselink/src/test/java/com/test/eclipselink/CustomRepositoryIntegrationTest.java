package com.test.eclipselink;

import com.test.eclipselink.model.User;
import com.test.eclipselink.repo.CustomUserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;


@SpringBootTest
@Sql(scripts = "/create-data.sql")
@Sql(scripts = "/cleanup-data.sql", executionPhase = AFTER_TEST_METHOD)
public class CustomRepositoryIntegrationTest {

	@Autowired
	private CustomUserRepository customUserRepository;

	@Test
	public void givenPerson_whenSave_thenAddOnePersonToDB() {
		User user = customUserRepository.customFindMethod(1L);
		assertEquals("naresh", user.getName());
	}



}