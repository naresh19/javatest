package com.test.eclipselink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EclipselinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(EclipselinkApplication.class, args);
	}

}
