package com.test.eclipselink.repo;

import com.test.eclipselink.model.User;

public interface CustomUserRepository {
    User customFindMethod(Long id);
}