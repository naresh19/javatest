package com.test.eclipselink.repo;

import org.springframework.stereotype.Repository;

import com.test.eclipselink.model.User;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
public class CustomUserRepositoryImpl implements CustomUserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public User customFindMethod(Long id) {
        return (User) entityManager.createQuery("FROM User u WHERE u.id = :id")
          .setParameter("id", id)
          .getSingleResult();

        //User user = entityManager.find(User.class, 10L);
        //return user;
    }

	
}