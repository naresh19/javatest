package com.test.eclipselink.repo;

import com.test.eclipselink.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonsRepository extends CrudRepository<Person, Long> {

    Person findByFirstName(String firstName);

}