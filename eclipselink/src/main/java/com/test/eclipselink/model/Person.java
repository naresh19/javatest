package com.test.eclipselink.model;

import jakarta.persistence.*;

@Entity
public class Person {

    @Id
    @SequenceGenerator(name="seq1",sequenceName="person_seq",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "seq1")
    private Long id;
    private String firstName;
    private String lastName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}