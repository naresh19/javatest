CREATE USER naresh IDENTIFIED BY test1234;
GRANT CONNECT, RESOURCE, DBA TO naresh;
GRANT CREATE SESSION TO naresh WITH ADMIN OPTION;

 
CREATE TABLE project
  (
    project_id          NUMBER(19,0) NOT NULL,
    end_date            TIMESTAMP,
    name                VARCHAR2(255 CHAR),
    responsible_manager VARCHAR2(255 CHAR),
    start_date          TIMESTAMP,
    PRIMARY KEY (project_id)
  )
  
  
CREATE TABLE activities
  (
    activity_id NUMBER(19,0) NOT NULL,
    name        VARCHAR2(255 CHAR),
    project_id  NUMBER(19,0),
    PRIMARY KEY (activity_id),
    constraint FK_PROJECT_ID foreign key (project_id) references project
  )
  
  
  
CREATE TABLE MESSAGE
  (
    id      NUMBER(19,0) NOT NULL,
    MESSAGE VARCHAR2(255 CHAR) NOT NULL,
    PRIMARY KEY (id)
  )