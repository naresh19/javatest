package com.hibernate.demo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="project")
public class Project {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "project_id")
  private Long projectId;
  
  private String name;
  
  @Column(name = "responsible_manager")
  private String responsibleManager;
  
  @Column(name = "start_date")
  private Date startDate;
  
  @Column(name = "end_date")
  private Date endDate;
  
  @OneToMany(fetch=FetchType.LAZY,mappedBy="project",orphanRemoval=true,cascade = CascadeType.ALL)
  private List<Activity> activities = new ArrayList<Activity>();

  public Project() {}

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getResponsibleManager() {
    return responsibleManager;
  }

  public void setResponsibleManager(String responsibleManager) {
    this.responsibleManager = responsibleManager;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
  
  public void addActivity(Activity activity)
  {
    activities.add(activity);
    activity.setProject(this);
  }
  
  public void removeActivity(Activity activity)
  {
    activities.remove(activity);
    activity.setProject(null);
  }
  
  public List<Activity> getActivities()
  {
    return activities;
  }



}
