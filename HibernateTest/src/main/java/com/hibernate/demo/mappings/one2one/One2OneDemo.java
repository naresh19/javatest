package com.hibernate.demo.mappings.one2one;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;

import com.hibernate.demo.SessionFactoryHelper;
import com.hibernate.demo.mappings.one2one.entity.Aadhaar;
import com.hibernate.demo.mappings.one2one.entity.Person;

public class One2OneDemo {

	private static SessionFactory sessionFactory;

	static {
		sessionFactory = SessionFactoryHelper.getSessionFactory();
	}

	public static void main(String[] args)
	{
		clearData();
		Person person = savePerson();
		Long aadhaarid = saveAaadhaar(person);
		
		//load person data along with Aaadhaar Data
		Person p = loadPerson(person.getPersonId());
		System.out.println(p.getAadhaar());
		
	}
	
	  private static void clearData() {
		    try (Session session = sessionFactory.openSession()) {
		      Transaction tx = session.beginTransaction();
		      NativeQuery createSQLQuery = session.createSQLQuery("delete from aadhaar");
		      createSQLQuery.executeUpdate();
		      NativeQuery createSQLQuery2 = session.createSQLQuery("delete from person");
		      createSQLQuery2.executeUpdate();
		      tx.commit();
		    }
		  }

	private static Person loadPerson(Long personId) 
	{
		Session session = openSession();
		Transaction tx = session.beginTransaction();
		Person p = session.load(Person.class, personId);
		p.getAadhaar();
		tx.commit();
		session.close();
		return p;
	}

	private static Person savePerson() {
		Session session = openSession();
		Transaction tx = session.beginTransaction();
		Person p = new Person();
		p.setName("Naresh");
		session.save(p);
		tx.commit();
		return p;
	}

	private static Session openSession() {
		Session session = sessionFactory.openSession();
		return session;
	}
	
	
	private static Long saveAaadhaar(Person person) {
		Session session = openSession();
		Transaction tx = session.beginTransaction();
		Aadhaar a  = new Aadhaar();
		a.setAddress("BLR");
		a.setPerson(person);
		session.save(a);
		tx.commit();
		return a.getAadhaarId();
	}

}
