package com.hibernate.demo.mappings.one2one.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "aadhaar")
public class Aadhaar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "aadhaar_id")
	private Long aadhaarId;
	
	private String address;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
	private Person person;
	
	public Aadhaar() {
		// TODO Auto-generated constructor stub
	}

	public Long getAadhaarId() {
		return aadhaarId;
	}

	public void setAadhaarId(Long aadhaarId) {
		this.aadhaarId = aadhaarId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Aaadhaar Id:"+aadhaarId + ",Address:"+address;
	}
}
