package com.hibernate.demo.samples;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.demo.SessionFactoryHelper;
import com.hibernate.demo.entity.Message;

public class PersistenceTest {
	private static final SessionFactory factory;

	static {
		factory = SessionFactoryHelper.getSessionFactory();
	}

	public static void saveMessage() {
		Message message = new Message("Welcome!!");
		try (Session session = factory.openSession()) {
			Transaction tx = session.beginTransaction();
			session.persist(message);
			tx.commit();
		}
	}

	public static void readMessage() {
		try (Session session = factory.openSession()) {
			List<Message> list = session.createQuery("from Message", Message.class).list();

			for (Message m : list) {
				System.out.println(m.getMessage());
			}
		}
	}

	public static void main(String[] args) {

		saveMessage();
		readMessage();
		System.exit(0);
	}
}