package com.hibernate.demo.samples;

import java.util.Date;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.demo.SessionFactoryHelper;
import com.hibernate.demo.entity.Activity;
import com.hibernate.demo.entity.Project;

public class TestHibernateFeatures {
  private static final SessionFactory sessionFactory = SessionFactoryHelper.getSessionFactory();

  public static void main(String[] args) {


    // clear the data
    clearData(sessionFactory);

    //simpleOperations();
    
    //let's try saving and loading the project along with its activity using bidirectional mapping
    
    Project project1 = new Project();
    try (Session session = sessionFactory.openSession()) {
      project1.setName("test project");
      project1.setResponsibleManager("abcd");
      project1.setStartDate(new Date());
      project1.setEndDate(new Date());
      
      Activity a1 = new Activity();
      a1.setName("A1");
      project1.addActivity(a1);
      Transaction tx = session.beginTransaction();
      session.persist(project1);
      tx.commit();
    }
    
    Project p1 = null;
    try (Session session = sessionFactory.openSession()) {
      p1 = session.load(Project.class, project1.getProjectId());
      System.out.println("Project along with activity:"+p1.getActivities().get(0).getName());
    }
    
    System.out.println("Test Sucessfull");
  }

  private static void simpleOperations() {
    // SAVE PROJECT DATA - persist(...)
    Long projectId = saveProjectData("Project1", "Naresh", new Date(), new Date());
    System.out.println("Project with Id created-" + projectId);

    // LOAD THE PROJECT
    Project project = loadProject(projectId);
    System.out.println("Project loaded:" + project.getName() + "," + project.getStartDate());

    // UPDATE THE PROJECT
    project.setName("Updated Name");
    updateProject(project);
    

    // RE-LOAD THE PROJECT
    project = loadProject(projectId);
    System.out.println("Project loaded:" + project.getName() + "," + project.getStartDate());
  }

  private static Project loadProject(Long projectId) {
    Project p = null;
    try (Session session = sessionFactory.openSession()) {
      p = session.load(Project.class, projectId);
      // workaround for lazy initialization exeption
      // if i dont call any property accessor method and directly return the object, since
      // it is proxy object, it lazily loads the data and requires session to load the data.
      // since session is closed here, the caller will get lazy init exception when accessing get
      // method
      p.getName();
      // p.getProjectId() will not work , use other accessor methods only
    }
    return p;
  }

  private static void updateProject(Project project) {
    try (Session session = sessionFactory.openSession()) {
      Transaction tx = session.beginTransaction();
      session.update(project);
      tx.commit();
    }
  }

  private static void clearData(SessionFactory sessionFactory) {
    try (Session session = sessionFactory.openSession()) {
      Transaction tx = session.beginTransaction();
      session.createSQLQuery("TRUNCATE hibernatedemo.activities");
      session.createSQLQuery("TRUNCATE hibernatedemo.project");
      tx.commit();
    }
  }

  private static Long saveProjectData(String name, String owner, Date sd, Date ed) {

    Project project = new Project();
    try (Session session = sessionFactory.openSession()) {
      project.setName(name);
      project.setResponsibleManager(owner);
      project.setStartDate(sd);
      project.setEndDate(ed);
      Transaction tx = session.beginTransaction();
      session.persist(project);
      tx.commit();
    }
    return project.getProjectId();

  }
  
  
  private static Long saveActivityData(String name, Long proj_id) {

    Activity activity = new Activity();
    try (Session session = sessionFactory.openSession()) {
      activity.setName(name);
      //activity.setProjectId(proj_id);
      activity.setProject(null);
      Transaction tx = session.beginTransaction();
      session.persist(activity);
      tx.commit();
    }
    return activity.getActivityId();

  }
  
  
  private static Activity loadActivity(Long activityId) {
    Activity a = null;
    try (Session session = sessionFactory.openSession()) {
      a = session.load(Activity.class, activityId);
      // workaround for lazy initialization exeption
      // if i dont call any property accessor method and directly return the object, since
      // it is proxy object, it lazily loads the data and requires session to load the data.
      // since session is closed here, the caller will get lazy init exception when accessing get
      // method
      a.getName();
      // p.getProjectId() will not work , use other accessor methods only
    }
    return a;
  }
}
