package differenceLib;
 
/**********************************************************************************************
 * Copyright (c) 2001-2012 Liquid Technologies Limited. All rights reserved.
 * See www.liquid-technologies.com for product details.
 *
 * Please see products End User License Agreement for distribution permissions.
 *
 * WARNING: THIS FILE IS GENERATED
 * Changes made outside of ##HAND_CODED_BLOCK_START blocks will be overwritten
 *
 * Generation  : by Liquid XML Data Binder 10.1.7.4256
 * Using Schema: I:/EclipseGalileoWS/JavaTest/difference.xsd
 **********************************************************************************************/
import java.io.IOException;

	
/// <summary>
/// 	This class represents the Element Difference
/// </summary>
public class Difference extends com.liquid_technologies.ltxmllib10.XmlGeneratedClass
{

	/// <summary>
	/// 	Constructor for Difference
	/// </summary>
	/// <remarks>
	/// 	<BR>The class is created with all the mandatory fields populated with the
	///		default data. </BR>
	/// 	<BR>All Collection object are created.</BR>
	///		<BR>However any 1-n relationships (these are represented as collections) are
	///		empty. To comply with the schema these must be populated before the xml
	///		obtained from ToXml is valid against the schema I:/EclipseGalileoWS/JavaTest/difference.xsd</BR>
	/// </remarks>
	public Difference()
	{
		setElementName("Difference");
		init();
	}
	public Difference(String elementName)
	{
		setElementName(elementName);
		init();
	}		

	/// <summary>
	/// 	Initilizes the class
	/// </summary>
	/// <remarks>
	/// 	<BR>The Creates all the mandatory fields (populated with the default data) 
	/// 	All Collection object are created.</BR>
	///		<BR>However any 1-n relationships (these are represented as collections) are
	///		empty. To comply with the schema these must be populated before the xml
	///		obtained from ToXml is valid against the schema I:/EclipseGalileoWS/JavaTest/difference.xsd.</BR>
	/// </remarks>
	protected void init()
	{
		try
		{
			differenceLib.Registration.iRegistrationIndicator = 0; // causes registration to take place
			_leftSideAdditionalEntries = new differenceLib.LeftSideAdditionalEntriesType("LeftSideAdditionalEntries");
			_rightSideAdditionalEntries = new differenceLib.RightSideAdditionalEntriesType("RightSideAdditionalEntries");
			_conflicts = new differenceLib.ConflictType("Conflicts");


			// ##HAND_CODED_BLOCK_START ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
			// Add Additional initilization code here...
			// ##HAND_CODED_BLOCK_END ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

			getClassAttributeInfo();
			getClassElementInfo();
			
			if (1==0)
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException(""); // dummy throw to shut compiler up
		} catch (com.liquid_technologies.ltxmllib10.exceptions.LtException e) {
			// should never happen - perhaps a default value is invalid in the schema?
			e.printStackTrace();
			throw new InternalError();
		} catch (Exception ex) {
			// should never happen
			ex.printStackTrace();
			throw new InternalError();
		}
	}



	/// <summary>
	/// 	Allows the class to be copied
	/// </summary>
	/// <remarks>
	///		Performs a 'deep copy' of all the data in the class (and its children)
	/// </remarks>
	public /*override*/ Object clone() throws CloneNotSupportedException
	{
		try
		{
			differenceLib.Difference newObject = (differenceLib.Difference)super.clone();

			// clone, creates a bitwise copy of the class, so all the collections are the
			// same as the parents. Init will re-create our own collections, and classes, 
			// preventing objects being shared between the new an original objects
			newObject.init();
			newObject._leftSideAdditionalEntries = null;
			if (_leftSideAdditionalEntries != null)
				newObject._leftSideAdditionalEntries = (differenceLib.LeftSideAdditionalEntriesType)_leftSideAdditionalEntries.clone();
			newObject._rightSideAdditionalEntries = null;
			if (_rightSideAdditionalEntries != null)
				newObject._rightSideAdditionalEntries = (differenceLib.RightSideAdditionalEntriesType)_rightSideAdditionalEntries.clone();
			newObject._conflicts = null;
			if (_conflicts != null)
				newObject._conflicts = (differenceLib.ConflictType)_conflicts.clone();
	
// ##HAND_CODED_BLOCK_START ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional clone code here...

// ##HAND_CODED_BLOCK_END ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

			if (1==0)
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException(""); // dummy throw to shut compiler up

			return newObject;
		} catch (CloneNotSupportedException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		} catch (com.liquid_technologies.ltxmllib10.exceptions.LtException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		}
	}
				
	public /*override*/ String getTargetNamespace()
	{
		return "";
	}

	/// <summary>
	///		Represents a mandatory Element in the XML document
	/// 	
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Element in the XML.</BR>
	/// 	<BR>It is mandatory and therefore must be populated within the XML.</BR>
	///		<BR>If this property is set, then the object will be COPIED. If the property is set to null an exception is raised.</BR>
	/// </remarks>
	public differenceLib.LeftSideAdditionalEntriesType getLeftSideAdditionalEntries()
	{
		return _leftSideAdditionalEntries;  
	}
	public void setLeftSideAdditionalEntries(differenceLib.LeftSideAdditionalEntriesType value) throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		throw_IfPropertyIsNull(value, "LeftSideAdditionalEntries");
		if (value != null)
			setElementName(value.getBase(), "LeftSideAdditionalEntries");
//		throw_IfElementNameDiffers(value, "LeftSideAdditionalEntries");
		_leftSideAdditionalEntries = value; 
	}
	protected differenceLib.LeftSideAdditionalEntriesType _leftSideAdditionalEntries;

	/// <summary>
	///		Represents a mandatory Element in the XML document
	/// 	
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Element in the XML.</BR>
	/// 	<BR>It is mandatory and therefore must be populated within the XML.</BR>
	///		<BR>If this property is set, then the object will be COPIED. If the property is set to null an exception is raised.</BR>
	/// </remarks>
	public differenceLib.RightSideAdditionalEntriesType getRightSideAdditionalEntries()
	{
		return _rightSideAdditionalEntries;  
	}
	public void setRightSideAdditionalEntries(differenceLib.RightSideAdditionalEntriesType value) throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		throw_IfPropertyIsNull(value, "RightSideAdditionalEntries");
		if (value != null)
			setElementName(value.getBase(), "RightSideAdditionalEntries");
//		throw_IfElementNameDiffers(value, "RightSideAdditionalEntries");
		_rightSideAdditionalEntries = value; 
	}
	protected differenceLib.RightSideAdditionalEntriesType _rightSideAdditionalEntries;

	/// <summary>
	///		Represents a mandatory Element in the XML document
	/// 	
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Element in the XML.</BR>
	/// 	<BR>It is mandatory and therefore must be populated within the XML.</BR>
	///		<BR>If this property is set, then the object will be COPIED. If the property is set to null an exception is raised.</BR>
	/// </remarks>
	public differenceLib.ConflictType getConflicts()
	{
		return _conflicts;  
	}
	public void setConflicts(differenceLib.ConflictType value) throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		throw_IfPropertyIsNull(value, "Conflicts");
		if (value != null)
			setElementName(value.getBase(), "Conflicts");
//		throw_IfElementNameDiffers(value, "Conflicts");
		_conflicts = value; 
	}
	protected differenceLib.ConflictType _conflicts;

	public String getNamespace()
	{
		return "";
	}	

	public /*override*/ com.liquid_technologies.ltxmllib10.XmlObjectBase	getBase()
	{
		return this;
	}
	protected void onEvent(com.liquid_technologies.ltxmllib10.XmlObjectBase msgSource, int msgType, Object data) 
		throws com.liquid_technologies.ltxmllib10.exceptions.LtInvalidStateException
	{
		if (msgType == CollectionChangeEvent)
		{
		    if (0 == 1)
		    {
		    }
		}
	}

	private static com.liquid_technologies.ltxmllib10.ParentElementInfo _parentElementInfo = null;
	private static com.liquid_technologies.ltxmllib10.ElementInfo[] _elementInfo = null;
	private static com.liquid_technologies.ltxmllib10.AttributeInfo[] _attributeInfo = null;
		
	protected com.liquid_technologies.ltxmllib10.ParentElementInfo getClassInfo()
		throws Exception
	{
		if (_parentElementInfo == null)
		{
			_parentElementInfo = new com.liquid_technologies.ltxmllib10.ParentElementInfo(	
																	com.liquid_technologies.ltxmllib10.XmlElementGroupType.Sequence,
																	com.liquid_technologies.ltxmllib10.XmlElementType.Element,
																	"Difference",
																	"",
																	true,
																	false,
																	null,
																	null,
																	com.liquid_technologies.ltxmllib10.ConversionType.type_None,
																	null,
																	false);
		}
		return _parentElementInfo;
	}

	protected com.liquid_technologies.ltxmllib10.ElementInfo[] getClassElementInfo()
		throws Exception
	{
		if (_elementInfo == null)
		{
			_elementInfo = new com.liquid_technologies.ltxmllib10.ElementInfo[]
			{
				 new com.liquid_technologies.ltxmllib10.data.ElementInfoSeqClsMnd("LeftSideAdditionalEntries", "", findGetterMethod("differenceLib.Difference", "getLeftSideAdditionalEntries"), findSetterMethod("differenceLib.Difference", "setLeftSideAdditionalEntries", "differenceLib.LeftSideAdditionalEntriesType"), com.liquid_technologies.ltxmllib10.XmlElementType.Element, Class.forName("differenceLib.LeftSideAdditionalEntriesType"), false)
				,new com.liquid_technologies.ltxmllib10.data.ElementInfoSeqClsMnd("RightSideAdditionalEntries", "", findGetterMethod("differenceLib.Difference", "getRightSideAdditionalEntries"), findSetterMethod("differenceLib.Difference", "setRightSideAdditionalEntries", "differenceLib.RightSideAdditionalEntriesType"), com.liquid_technologies.ltxmllib10.XmlElementType.Element, Class.forName("differenceLib.RightSideAdditionalEntriesType"), false)
				,new com.liquid_technologies.ltxmllib10.data.ElementInfoSeqClsMnd("Conflicts", "", findGetterMethod("differenceLib.Difference", "getConflicts"), findSetterMethod("differenceLib.Difference", "setConflicts", "differenceLib.ConflictType"), com.liquid_technologies.ltxmllib10.XmlElementType.Element, Class.forName("differenceLib.ConflictType"), false)
			};
		}
		return _elementInfo;
	}

	protected com.liquid_technologies.ltxmllib10.AttributeInfo[] getClassAttributeInfo()
		throws Exception
	{
		if (_attributeInfo==null)
		{
			_attributeInfo = new com.liquid_technologies.ltxmllib10.AttributeInfo[]
			{
			};
		}
		return _attributeInfo;
	}

// ##HAND_CODED_BLOCK_START ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional Methods and members here...

// ##HAND_CODED_BLOCK_END ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
}



