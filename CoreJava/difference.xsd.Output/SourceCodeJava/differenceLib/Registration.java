package differenceLib;

/**********************************************************************************************
 * Copyright (c) 2001-2012 Liquid Technologies Limited. All rights reserved.
 * See www.liquid-technologies.com for product details.
 *
 * Please see products End User License Agreement for distribution permissions.
 *
 * WARNING: THIS FILE IS GENERATED
 * Changes made outside of ##HAND_CODED_BLOCK_START blocks will be overwritten
 *
 * Generation  : by Liquid XML Data Binder 10.1.7.4256
 * Using Schema: I:/EclipseGalileoWS/JavaTest/difference.xsd
 **********************************************************************************************/

public class Registration
{
	private static int registerLicense()
	{
		com.liquid_technologies.ltxmllib10.XmlObjectBase.register("Trial 30/12/2012", "difference.xsd", "BC0Y7RGJKBDQXELH000000AA");
// ##HAND_CODED_BLOCK_START ID="Namespace Declarations"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
// Add Additional namespace declarations here...
			com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default.setSchemaType(com.liquid_technologies.ltxmllib10.SchemaType.XSD);
//			com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default.setDefaultNamespaceURI("http://www.fpml.org/2003/FpML-4-0");
//			com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default.getNamespaceAliases().add("dsig", "http://www.w3.org/2000/09/xmldsig#");

			com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default.getNamespaceAliases().add("xs", "http://www.w3.org/2001/XMLSchema-instance");

// ##HAND_CODED_BLOCK_END ID="Namespace Declarations"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
		return 1;
	}
	static public int iRegistrationIndicator = registerLicense();
}


