package differenceLib;
 
/**********************************************************************************************
 * Copyright (c) 2001-2012 Liquid Technologies Limited. All rights reserved.
 * See www.liquid-technologies.com for product details.
 *
 * Please see products End User License Agreement for distribution permissions.
 *
 * WARNING: THIS FILE IS GENERATED
 * Changes made outside of ##HAND_CODED_BLOCK_START blocks will be overwritten
 *
 * Generation  : by Liquid XML Data Binder 10.1.7.4256
 * Using Schema: I:/EclipseGalileoWS/JavaTest/difference.xsd
 **********************************************************************************************/
import java.io.IOException;

	
/// <summary>
/// 	This class represents the ComplexType EmployeeType
/// </summary>
public class EmployeeType extends com.liquid_technologies.ltxmllib10.XmlGeneratedClass
{

	/// <summary>
	/// 	Constructor for EmployeeType
	/// </summary>
	/// <remarks>
	/// 	<BR>The class is created with all the mandatory fields populated with the
	///		default data. </BR>
	/// 	<BR>All Collection object are created.</BR>
	///		<BR>However any 1-n relationships (these are represented as collections) are
	///		empty. To comply with the schema these must be populated before the xml
	///		obtained from ToXml is valid against the schema I:/EclipseGalileoWS/JavaTest/difference.xsd</BR>
	/// </remarks>
	public EmployeeType()
	{
		setElementName("EmployeeType");
		init();
	}
	public EmployeeType(String elementName)
	{
		setElementName(elementName);
		init();
	}		

	/// <summary>
	/// 	Initilizes the class
	/// </summary>
	/// <remarks>
	/// 	<BR>The Creates all the mandatory fields (populated with the default data) 
	/// 	All Collection object are created.</BR>
	///		<BR>However any 1-n relationships (these are represented as collections) are
	///		empty. To comply with the schema these must be populated before the xml
	///		obtained from ToXml is valid against the schema I:/EclipseGalileoWS/JavaTest/difference.xsd.</BR>
	/// </remarks>
	protected void init()
	{
		try
		{
			differenceLib.Registration.iRegistrationIndicator = 0; // causes registration to take place
			_id = "";
			_isValidId = false;
			_name = "";
			_deptId = "";


			// ##HAND_CODED_BLOCK_START ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
			// Add Additional initilization code here...
			// ##HAND_CODED_BLOCK_END ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

			getClassAttributeInfo();
			getClassElementInfo();
			
			if (1==0)
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException(""); // dummy throw to shut compiler up
		} catch (com.liquid_technologies.ltxmllib10.exceptions.LtException e) {
			// should never happen - perhaps a default value is invalid in the schema?
			e.printStackTrace();
			throw new InternalError();
		} catch (Exception ex) {
			// should never happen
			ex.printStackTrace();
			throw new InternalError();
		}
	}



	/// <summary>
	/// 	Allows the class to be copied
	/// </summary>
	/// <remarks>
	///		Performs a 'deep copy' of all the data in the class (and its children)
	/// </remarks>
	public /*override*/ Object clone() throws CloneNotSupportedException
	{
		try
		{
			differenceLib.EmployeeType newObject = (differenceLib.EmployeeType)super.clone();

			// clone, creates a bitwise copy of the class, so all the collections are the
			// same as the parents. Init will re-create our own collections, and classes, 
			// preventing objects being shared between the new an original objects
			newObject.init();
			if (_isValidId)
				newObject._id = _id;
			newObject._isValidId = _isValidId;
			newObject._name = _name;
			newObject._deptId = _deptId;
	
// ##HAND_CODED_BLOCK_START ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional clone code here...

// ##HAND_CODED_BLOCK_END ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

			if (1==0)
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException(""); // dummy throw to shut compiler up

			return newObject;
		} catch (CloneNotSupportedException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		} catch (com.liquid_technologies.ltxmllib10.exceptions.LtException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		}
	}
				
	public /*override*/ String getTargetNamespace()
	{
		return "";
	}

	/// <summary>
	///		Represents an optional Attribute in the XML document
	/// 	
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Attribute in the XML.</BR>
	/// 	<BR>It is optional, initially it is not valid.</BR>
	/// </remarks>
	public java.lang.String getId() throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		if (_isValidId == false)
			throw new com.liquid_technologies.ltxmllib10.exceptions.LtInvalidStateException("The Property Id is not valid. Set IdValid = true");
		return _id;  
	}
	public void setId(java.lang.String value) throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		// Apply whitespace rules appropriatley
		value = com.liquid_technologies.ltxmllib10.WhitespaceUtils.preserve(value); 
		_isValidId = true;
		_id = value; 
	}

	/// <summary>
	/// 	Indicates if Id contains a valid value.
	/// </summary>
	/// <remarks>
	/// 	<BR>true if the value for Id is valid, false if not.</BR>
	///		<BR>If this is set to true then the property is considered valid, and assigned its
	///		default value ("").</BR>
	///		<BR>If its set to false then its made invalid, and susiquent calls to get Id
	///     will raise an exception.</BR>
	/// </remarks>
	public boolean isValidId()
	{
		return _isValidId;
	}
	public void setValidId(boolean value)  throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		if (value != _isValidId)
		{
			setId("");
			_isValidId = value;
		}
	}
	protected boolean _isValidId;
	protected java.lang.String _id;

	/// <summary>
	///		Represents a mandatory Element in the XML document
	/// 	
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Element in the XML.</BR>
	/// 	<BR>It is mandatory and therefore must be populated within the XML.</BR>
	/// 	<BR>It is defaulted to "".</BR>
	/// </remarks>
	public java.lang.String getName() throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
		return _name;  
	}
	public void setName(java.lang.String value) throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		// Apply whitespace rules appropriatley
		value = com.liquid_technologies.ltxmllib10.WhitespaceUtils.preserve(value); 
		_name = value; 
	}
	protected java.lang.String _name;


	/// <summary>
	///		Represents a mandatory Element in the XML document
	/// 	
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Element in the XML.</BR>
	/// 	<BR>It is mandatory and therefore must be populated within the XML.</BR>
	/// 	<BR>It is defaulted to "".</BR>
	/// </remarks>
	public java.lang.String getDeptId() throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
		return _deptId;  
	}
	public void setDeptId(java.lang.String value) throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{ 
		// Apply whitespace rules appropriatley
		value = com.liquid_technologies.ltxmllib10.WhitespaceUtils.preserve(value); 
		_deptId = value; 
	}
	protected java.lang.String _deptId;


	public String getNamespace()
	{
		return "";
	}	

	public /*override*/ com.liquid_technologies.ltxmllib10.XmlObjectBase	getBase()
	{
		return this;
	}
	protected void onEvent(com.liquid_technologies.ltxmllib10.XmlObjectBase msgSource, int msgType, Object data) 
		throws com.liquid_technologies.ltxmllib10.exceptions.LtInvalidStateException
	{
		if (msgType == CollectionChangeEvent)
		{
		    if (0 == 1)
		    {
		    }
		}
	}

	private static com.liquid_technologies.ltxmllib10.ParentElementInfo _parentElementInfo = null;
	private static com.liquid_technologies.ltxmllib10.ElementInfo[] _elementInfo = null;
	private static com.liquid_technologies.ltxmllib10.AttributeInfo[] _attributeInfo = null;
		
	protected com.liquid_technologies.ltxmllib10.ParentElementInfo getClassInfo()
		throws Exception
	{
		if (_parentElementInfo == null)
		{
			_parentElementInfo = new com.liquid_technologies.ltxmllib10.ParentElementInfo(	
																	com.liquid_technologies.ltxmllib10.XmlElementGroupType.Sequence,
																	com.liquid_technologies.ltxmllib10.XmlElementType.Element,
																	"EmployeeType",
																	"",
																	true,
																	false,
																	null,
																	null,
																	com.liquid_technologies.ltxmllib10.ConversionType.type_None,
																	null,
																	false);
		}
		return _parentElementInfo;
	}

	protected com.liquid_technologies.ltxmllib10.ElementInfo[] getClassElementInfo()
		throws Exception
	{
		if (_elementInfo == null)
		{
			_elementInfo = new com.liquid_technologies.ltxmllib10.ElementInfo[]
			{
				 new com.liquid_technologies.ltxmllib10.data.ElementInfoSeqPrimMnd("Name", "", findGetterMethod("differenceLib.EmployeeType", "getName"), findSetterMethod("differenceLib.EmployeeType", "setName", "java.lang.String"), null, null, com.liquid_technologies.ltxmllib10.ConversionType.type_String, null, com.liquid_technologies.ltxmllib10.WhitespaceRule.Preserve, new com.liquid_technologies.ltxmllib10.PrimitiveRestrictions("", -1, -1, "", "", "", "", -1))
				,new com.liquid_technologies.ltxmllib10.data.ElementInfoSeqPrimMnd("DeptId", "", findGetterMethod("differenceLib.EmployeeType", "getDeptId"), findSetterMethod("differenceLib.EmployeeType", "setDeptId", "java.lang.String"), null, null, com.liquid_technologies.ltxmllib10.ConversionType.type_String, null, com.liquid_technologies.ltxmllib10.WhitespaceRule.Preserve, new com.liquid_technologies.ltxmllib10.PrimitiveRestrictions("", -1, -1, "", "", "", "", -1))
			};
		}
		return _elementInfo;
	}

	protected com.liquid_technologies.ltxmllib10.AttributeInfo[] getClassAttributeInfo()
		throws Exception
	{
		if (_attributeInfo==null)
		{
			_attributeInfo = new com.liquid_technologies.ltxmllib10.AttributeInfo[]
			{
				 new com.liquid_technologies.ltxmllib10.AttributeInfoPrimitive("Id", "", findGetterMethod("differenceLib.EmployeeType", "getId"), findSetterMethod("differenceLib.EmployeeType", "setId", "java.lang.String"), findGetterMethod("differenceLib.EmployeeType", "isValidId"), com.liquid_technologies.ltxmllib10.ConversionType.type_String, null, com.liquid_technologies.ltxmllib10.WhitespaceRule.Preserve, new com.liquid_technologies.ltxmllib10.PrimitiveRestrictions("", -1, -1, "", "", "", "", -1), null)
			};
		}
		return _attributeInfo;
	}

// ##HAND_CODED_BLOCK_START ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional Methods and members here...

// ##HAND_CODED_BLOCK_END ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
}



