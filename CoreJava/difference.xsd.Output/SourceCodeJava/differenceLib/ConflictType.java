package differenceLib;
 
/**********************************************************************************************
 * Copyright (c) 2001-2012 Liquid Technologies Limited. All rights reserved.
 * See www.liquid-technologies.com for product details.
 *
 * Please see products End User License Agreement for distribution permissions.
 *
 * WARNING: THIS FILE IS GENERATED
 * Changes made outside of ##HAND_CODED_BLOCK_START blocks will be overwritten
 *
 * Generation  : by Liquid XML Data Binder 10.1.7.4256
 * Using Schema: I:/EclipseGalileoWS/JavaTest/difference.xsd
 **********************************************************************************************/
import java.io.IOException;

	
/// <summary>
/// 	This class represents the ComplexType ConflictType
/// </summary>
public class ConflictType extends com.liquid_technologies.ltxmllib10.XmlGeneratedClass
{

	/// <summary>
	/// 	Constructor for ConflictType
	/// </summary>
	/// <remarks>
	/// 	<BR>The class is created with all the mandatory fields populated with the
	///		default data. </BR>
	/// 	<BR>All Collection object are created.</BR>
	///		<BR>However any 1-n relationships (these are represented as collections) are
	///		empty. To comply with the schema these must be populated before the xml
	///		obtained from ToXml is valid against the schema I:/EclipseGalileoWS/JavaTest/difference.xsd</BR>
	/// </remarks>
	public ConflictType()
	{
		setElementName("ConflictType");
		init();
	}
	public ConflictType(String elementName)
	{
		setElementName(elementName);
		init();
	}		

	/// <summary>
	/// 	Initilizes the class
	/// </summary>
	/// <remarks>
	/// 	<BR>The Creates all the mandatory fields (populated with the default data) 
	/// 	All Collection object are created.</BR>
	///		<BR>However any 1-n relationships (these are represented as collections) are
	///		empty. To comply with the schema these must be populated before the xml
	///		obtained from ToXml is valid against the schema I:/EclipseGalileoWS/JavaTest/difference.xsd.</BR>
	/// </remarks>
	protected void init()
	{
		try
		{
			differenceLib.Registration.iRegistrationIndicator = 0; // causes registration to take place
			_conflictingData = new differenceLib.ConflictingDataTypeCol("ConflictingData", "", 0, -1);


			// ##HAND_CODED_BLOCK_START ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
			// Add Additional initilization code here...
			// ##HAND_CODED_BLOCK_END ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

			getClassAttributeInfo();
			getClassElementInfo();
			
			if (1==0)
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException(""); // dummy throw to shut compiler up
		} catch (com.liquid_technologies.ltxmllib10.exceptions.LtException e) {
			// should never happen - perhaps a default value is invalid in the schema?
			e.printStackTrace();
			throw new InternalError();
		} catch (Exception ex) {
			// should never happen
			ex.printStackTrace();
			throw new InternalError();
		}
	}



	/// <summary>
	/// 	Allows the class to be copied
	/// </summary>
	/// <remarks>
	///		Performs a 'deep copy' of all the data in the class (and its children)
	/// </remarks>
	public /*override*/ Object clone() throws CloneNotSupportedException
	{
		try
		{
			differenceLib.ConflictType newObject = (differenceLib.ConflictType)super.clone();

			// clone, creates a bitwise copy of the class, so all the collections are the
			// same as the parents. Init will re-create our own collections, and classes, 
			// preventing objects being shared between the new an original objects
			newObject.init();
			for(int i=0; i<_conflictingData.count(); i++)
				newObject._conflictingData.add((differenceLib.ConflictingDataType)_conflictingData.getItem(i).clone());
	
// ##HAND_CODED_BLOCK_START ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional clone code here...

// ##HAND_CODED_BLOCK_END ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

			if (1==0)
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException(""); // dummy throw to shut compiler up

			return newObject;
		} catch (CloneNotSupportedException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		} catch (com.liquid_technologies.ltxmllib10.exceptions.LtException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		}
	}
				
	public /*override*/ String getTargetNamespace()
	{
		return "";
	}

	/// <summary>
	/// 	A collection of ConflictingDatas
	///		
	/// </summary>
	/// <remarks>
	/// 	<BR></BR>
	/// 	<BR>This property is represented as an Element in the XML.</BR>
	/// 	<BR>This collection may contain 0 to Many objects.</BR>
	/// </remarks>
	public differenceLib.ConflictingDataTypeCol getConflictingData()
	{
		return _conflictingData; 
	}
	protected	differenceLib.ConflictingDataTypeCol	_conflictingData;

	public String getNamespace()
	{
		return "";
	}	

	public /*override*/ com.liquid_technologies.ltxmllib10.XmlObjectBase	getBase()
	{
		return this;
	}
	protected void onEvent(com.liquid_technologies.ltxmllib10.XmlObjectBase msgSource, int msgType, Object data) 
		throws com.liquid_technologies.ltxmllib10.exceptions.LtInvalidStateException
	{
		if (msgType == CollectionChangeEvent)
		{
		    if (0 == 1)
		    {
		    }
		}
	}

	private static com.liquid_technologies.ltxmllib10.ParentElementInfo _parentElementInfo = null;
	private static com.liquid_technologies.ltxmllib10.ElementInfo[] _elementInfo = null;
	private static com.liquid_technologies.ltxmllib10.AttributeInfo[] _attributeInfo = null;
		
	protected com.liquid_technologies.ltxmllib10.ParentElementInfo getClassInfo()
		throws Exception
	{
		if (_parentElementInfo == null)
		{
			_parentElementInfo = new com.liquid_technologies.ltxmllib10.ParentElementInfo(	
																	com.liquid_technologies.ltxmllib10.XmlElementGroupType.Sequence,
																	com.liquid_technologies.ltxmllib10.XmlElementType.Element,
																	"ConflictType",
																	"",
																	true,
																	false,
																	null,
																	null,
																	com.liquid_technologies.ltxmllib10.ConversionType.type_None,
																	null,
																	false);
		}
		return _parentElementInfo;
	}

	protected com.liquid_technologies.ltxmllib10.ElementInfo[] getClassElementInfo()
		throws Exception
	{
		if (_elementInfo == null)
		{
			_elementInfo = new com.liquid_technologies.ltxmllib10.ElementInfo[]
			{
				 new com.liquid_technologies.ltxmllib10.data.ElementInfoSeqClsCol("ConflictingData", "", findGetterMethod("differenceLib.ConflictType", "getConflictingData"), com.liquid_technologies.ltxmllib10.XmlElementType.Element)
			};
		}
		return _elementInfo;
	}

	protected com.liquid_technologies.ltxmllib10.AttributeInfo[] getClassAttributeInfo()
		throws Exception
	{
		if (_attributeInfo==null)
		{
			_attributeInfo = new com.liquid_technologies.ltxmllib10.AttributeInfo[]
			{
			};
		}
		return _attributeInfo;
	}

// ##HAND_CODED_BLOCK_START ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional Methods and members here...

// ##HAND_CODED_BLOCK_END ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
}



