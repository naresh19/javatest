package differenceLib;

/**********************************************************************************************
 * Copyright (c) 2001-2012 Liquid Technologies Limited. All rights reserved.
 * See www.liquid-technologies.com for product details.
 *
 * Please see products End User License Agreement for distribution permissions.
 *
 * WARNING: THIS FILE IS GENERATED
 * Changes made outside of ##HAND_CODED_BLOCK_START blocks will be overwritten
 *
 * Generation  : by Liquid XML Data Binder 10.1.7.4256
 * Using Schema: I:/EclipseGalileoWS/JavaTest/difference.xsd
 **********************************************************************************************/
public final class ClassFactory extends com.liquid_technologies.ltxmllib10.ClassFactoryBase
{
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXml( String xmlIn )
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			return fromXml( xmlIn, com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default );
		}
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXml( String xmlIn, com.liquid_technologies.ltxmllib10.XmlSerializationContext context )
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			com.liquid_technologies.ltxmllib10.dom.XmlDocument parser = new com.liquid_technologies.ltxmllib10.dom.XmlDocument(context);
			parser.parse(new java.io.ByteArrayInputStream(xmlIn.getBytes()));
			return fromXmlElement(parser.getDocumentElement(), context);
		}
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXmlFile( String FileName )
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			return fromXmlFile(FileName, com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default);
		}
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXmlFile( String FileName, com.liquid_technologies.ltxmllib10.XmlSerializationContext context )
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			com.liquid_technologies.ltxmllib10.dom.XmlDocument parser = new com.liquid_technologies.ltxmllib10.dom.XmlDocument(context);
			parser.parse(FileName);
			return fromXmlElement(parser.getDocumentElement(), context);
		}

		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXmlStream(byte[] data)
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			return fromXmlStream(data, com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default);
		}
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXmlStream(byte[] data, com.liquid_technologies.ltxmllib10.XmlSerializationContext context)
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			com.liquid_technologies.ltxmllib10.dom.XmlDocument parser = new com.liquid_technologies.ltxmllib10.dom.XmlDocument(context);
			java.io.ByteArrayInputStream baStream = new java.io.ByteArrayInputStream(data);
			parser.parse(baStream);
			return fromXmlElement(parser.getDocumentElement(), context);
		}

		
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXmlElement(com.liquid_technologies.ltxmllib10.dom.XmlElement xmlParent)
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			return fromXmlElement(xmlParent, com.liquid_technologies.ltxmllib10.XmlSerializationContext.Default);
		}
		static public com.liquid_technologies.ltxmllib10.XmlObjectBase fromXmlElement(com.liquid_technologies.ltxmllib10.dom.XmlElement xmlParent, com.liquid_technologies.ltxmllib10.XmlSerializationContext context)
			throws com.liquid_technologies.ltxmllib10.exceptions.LtException, java.io.IOException
		{
			com.liquid_technologies.ltxmllib10.XmlObjectBase retVal = null;
			String elementName;
			String elementNamespaceUri;


			// Get the type name this is either 
			// from the element ie <Parent>... = Parent
			// or from the type ie <Parent xsi:type="someNS:SomeElement">... = SomeElement
			if (getElementType(xmlParent).equals(""))
			{
				elementName = xmlParent.getLocalName();
				elementNamespaceUri = xmlParent.getNamespaceURI();
			}
			else
			{
				elementName = getElementType(xmlParent);
				elementNamespaceUri = getElementTypeNamespaceUri(xmlParent);
			}

			// create the appropriate object
			if (elementName == null || elementName == "")
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException("The element to load has no name"); 
			if (retVal == null && elementName.equals("ConflictingDataType") && elementNamespaceUri.equals(""))
				retVal = new differenceLib.ConflictingDataType();
			if (retVal == null && elementName.equals("ConflictType") && elementNamespaceUri.equals(""))
				retVal = new differenceLib.ConflictType();
			if (retVal == null && elementName.equals("Difference") && elementNamespaceUri.equals(""))
				retVal = new differenceLib.Difference();
			if (retVal == null && elementName.equals("EmployeeType") && elementNamespaceUri.equals(""))
				retVal = new differenceLib.EmployeeType();
			if (retVal == null && elementName.equals("LeftSideAdditionalEntriesType") && elementNamespaceUri.equals(""))
				retVal = new differenceLib.LeftSideAdditionalEntriesType();
			if (retVal == null && elementName.equals("RightSideAdditionalEntriesType") && elementNamespaceUri.equals(""))
				retVal = new differenceLib.RightSideAdditionalEntriesType();
			if (retVal == null)
			{
				throw new com.liquid_technologies.ltxmllib10.exceptions.LtException("Failed load the element " + elementName + ":" + elementNamespaceUri + ". No appropriate class exists to load the data into. Ensure that the XML document complies with the schema.");
			}
			
			// load the data into the object
			retVal.fromXmlElement(xmlParent, context);

			return retVal;
		}



}

