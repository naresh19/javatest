/**********************************************************************************************
 * Copyright (c) 2001-2012 Liquid Technologies Limited. All rights reserved.
 * See www.liquid-technologies.com for product details.
 *
 * Please see products End User License Agreement for distribution permissions.
 *
 * WARNING: THIS FILE IS GENERATED
 * Changes made outside of ##HAND_CODED_BLOCK_START blocks will be overwritten
 *
 * Generation  : by Liquid XML Data Binder 10.1.7.4256
 * Using Schema: I:/EclipseGalileoWS/JavaTest/difference.xsd
 **********************************************************************************************/
package differenceLib;

import java.util.Iterator;
import java.io.IOException;


/// <summary>
/// 	This class represents a collection of ComplexTypes ConflictingDataType
/// </summary>
/// <remarks>
/// </remarks>
public class ConflictingDataTypeCol extends com.liquid_technologies.ltxmllib10.XmlCollectionBase
{
	// This should not be called directly
	public ConflictingDataTypeCol(String elementName, String targetNamespace, int minOccurs, int maxOccurs)
	{
		_targetNamespace 	= targetNamespace;
		setElementName(elementName);
		_minOccurs			= minOccurs;
		_maxOccurs			= maxOccurs;
// ##HAND_CODED_BLOCK_START ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional initilization code here...

// ##HAND_CODED_BLOCK_END ID="Additional Inits"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
	}

	/// <summary>
	/// 	Adds a ConflictingDataType enumeration to the collection
	/// </summary>
	/// <param name="newEnum">
	///		The object to be copied into the collection
	/// </param>
	/// <returns>
	///		The copied object (that was added to the collection)
	/// </returns>
	/// <remarks>
	/// 	The object newProperty, is copied, and the copy is added to the
	///		collection.
	/// </remarks>
	public differenceLib.ConflictingDataType	add(differenceLib.ConflictingDataType newCls)
	  throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
		return add(_coll.size(), newCls);
	}
	public differenceLib.ConflictingDataType	add(int index, differenceLib.ConflictingDataType newCls)
	  throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
	setElementName(newCls.getBase(), getElementName());
//		throw_IfElementNameDiffers(newCls, getElementName());
		_coll.add(index, newCls);
		fireOnCollectionChange();
		return newCls;
	}
	
	/// <summary>
	/// 	Adds a ConflictingDataType object to the collection
	/// </summary>
	/// <returns>
	///		The newly created ConflictingDataType object
	/// </returns>
	/// <remarks>
	/// 	A new ConflictingDataType object is created and added to the collection
	///		the new object is then returned.
	/// </remarks>
	public differenceLib.ConflictingDataType add()
	  throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
		return add(_coll.size());
	}
	public differenceLib.ConflictingDataType add(int index)
	  throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
		differenceLib.ConflictingDataType newObject = new differenceLib.ConflictingDataType(getElementName());
		_coll.add(index, newObject);
		fireOnCollectionChange();
		return newObject;
	}

	/// <summary>
	/// 	Gets a ConflictingDataType object from the collection
	/// </summary>
	/// <param name="index">
	///		The 0 based index of the item to retreve
	/// </param>
	/// <returns>
	///		The object at the given location in the collection
	/// </returns>
	public differenceLib.ConflictingDataType getItem(int index)
	{	
		return (differenceLib.ConflictingDataType) _coll.get(index);
	}

	/// <summary>
	/// 	Creates a copy of the collection
	/// </summary>
	/// <returns>
	///		The copied collection object
	/// </returns>
	/// <remarks>
	/// 	The function performs a 'deep copy' of the class, and all its children.
	/// </remarks>
	public Object clone() throws CloneNotSupportedException
	{
		try
		{
			ConflictingDataTypeCol newObject = (ConflictingDataTypeCol) super.clone();
			Iterator itr = _coll.iterator();
			while (itr.hasNext()) {
				newObject._coll.add(((differenceLib.ConflictingDataType)itr.next()).clone());
			}
// ##HAND_CODED_BLOCK_START ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional clone code here...

// ##HAND_CODED_BLOCK_END ID="Additional clone"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS
			return newObject;
		} catch (CloneNotSupportedException e) {
			// should never happen
			e.printStackTrace();
			throw new InternalError();
		}
	}
	
	protected void attributesToXml(
		com.liquid_technologies.ltxmllib10.XmlTextWriter xmlOut,
		com.liquid_technologies.ltxmllib10.XmlSerializationContext context)
		throws com.liquid_technologies.ltxmllib10.exceptions.LtException, IOException {
	}

	protected void toXml(
  		com.liquid_technologies.ltxmllib10.XmlTextWriter xmlOut, 
        boolean bRegisterNamespaces, 
        String namespaceUri,
		com.liquid_technologies.ltxmllib10.XmlSerializationContext context,
		boolean isOptionalChoice)
	throws com.liquid_technologies.ltxmllib10.exceptions.LtException, IOException
	{
		validateCount(context);
		
		Iterator itr = _coll.iterator();
		while (itr.hasNext()) 
		{
			differenceLib.ConflictingDataType oObj = (differenceLib.ConflictingDataType)itr.next();
			toXml(oObj.getBase(), xmlOut, false, getTargetNamespace(), context, isOptionalChoice);
		}
	}


	protected com.liquid_technologies.ltxmllib10.dom.XmlElement fromXml(
		com.liquid_technologies.ltxmllib10.dom.XmlElement xmlParent,
		com.liquid_technologies.ltxmllib10.dom.XmlElement xmlChild,
		com.liquid_technologies.ltxmllib10.XmlSerializationContext context,
		boolean isOptionalChoice)
		throws com.liquid_technologies.ltxmllib10.exceptions.LtException
	{
		// go through the nodes until we run out of ones that match
		while (xmlChild != null)
		{
			// Stop reading when we hit an element we can't deal with
			if (!CompareElement(context, xmlChild, getElementName(), _targetNamespace))
				break;
			differenceLib.ConflictingDataType newObj = new differenceLib.ConflictingDataType(getElementName());
			fromXml(newObj.getBase(), xmlChild, xmlChild.getFirstChildElement(), context);

			// Add new item to the collection
			_coll.add(newObj);
			fireOnCollectionChange();

			// Move to next node
			xmlChild = xmlChild.getNextSiblingElement();
		}
		return xmlChild;
	}

	// Attribute - Namespace
	public String getTargetNamespace()
	{
		return _targetNamespace; 
	}	
	public String getNamespace()
	{
		return _targetNamespace;
	}	

// ##HAND_CODED_BLOCK_START ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

// Add Additional Methods and members here...

// ##HAND_CODED_BLOCK_END ID="Additional Methods"## DO NOT MODIFY ANYTHING OUTSIDE OF THESE TAGS

	protected String _targetNamespace;	
}



