package com.huawei.datastructures;

import com.javatest.datastructures.sll.SingleLinkedList;

import junit.framework.TestCase;

public class TestSingleLinkedList_UT extends TestCase
{

	public void testShouldAddElementsToSLL() throws Exception
	{
		SingleLinkedList linkedList = new SingleLinkedList();
		linkedList.addNodeAtLast(0);
		linkedList.addNodeAtLast(1);
		linkedList.addNodeAtLast(2);
		linkedList.addNodeAtLast(3);
		
		int sum = linkedList.traverse();
		assertEquals(6, sum);
	}
	
	public void testFindTheMidElementInSLLInSinglePass() throws Exception
	{
		SingleLinkedList linkedList = new SingleLinkedList();
		linkedList.addNodeAtLast(0);
		linkedList.addNodeAtLast(1);
		linkedList.addNodeAtLast(2);
		linkedList.addNodeAtLast(3);
		linkedList.addNodeAtLast(4);
		
		int midElement = linkedList.findMidElement();
		assertEquals(2, midElement);
		
		String currentList = linkedList.getCurrentList();
		System.out.println(currentList);
	}
	
	public void testShouldReverseTheSLL() throws Exception
	{
		SingleLinkedList linkedList = new SingleLinkedList();
		linkedList.addNodeAtLast(0);
		linkedList.addNodeAtLast(1);
		linkedList.addNodeAtLast(2);
		linkedList.addNodeAtLast(3);
		linkedList.addNodeAtLast(4);
		
		linkedList.reverse();
		String currentList = linkedList.getCurrentList();
		System.out.println(currentList);
	}
	
	public void testShouldRemoveDuplicateElements() throws Exception
	{
		SingleLinkedList linkedList = new SingleLinkedList();
		linkedList.addNodeAtLast(0);
		linkedList.addNodeAtLast(1);
		linkedList.addNodeAtLast(0);
		linkedList.addNodeAtLast(3);
		linkedList.addNodeAtLast(3);
		
		linkedList.deleteDups();
		String currentList = linkedList.getCurrentList();
		System.out.println(currentList);
	}
	
}
