package com.javatest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EffortCalculator 
{

  public static void main(String[] args) 
  {
    List<Calendar> holidayList = new ArrayList<Calendar>(4);
    Calendar holiday = Calendar.getInstance();
    holiday.set(2019, Calendar.AUGUST, 07,0,0,0);
    holiday.set(Calendar.MILLISECOND, 0);
    holidayList.add(holiday);
    Calendar startDate = Calendar.getInstance();
    startDate.set(2019, Calendar.AUGUST, 01, 0, 0, 0);
    startDate.set(Calendar.MILLISECOND, 0);
    Calendar endDate = Calendar.getInstance();
    endDate.set(2019, Calendar.AUGUST, 10, 0, 0, 0);
    endDate.set(Calendar.MILLISECOND, 0);
    int totalEffort = 36;
    int noOfAvailableDaysToWork  = calculateEffort(startDate,endDate,totalEffort,holidayList);
    System.out.println("No of Available Days To Work:"+noOfAvailableDaysToWork);
    int effortPerDay = totalEffort/noOfAvailableDaysToWork;
    System.out.println("Effort per day:"+effortPerDay);
    
    Calendar queryDate = Calendar.getInstance();
    queryDate.set(2019, Calendar.AUGUST, 8,0,0,0);
    queryDate.set(Calendar.MILLISECOND, 0);
    
    int effortForTheGivenRange = getEffortFortheGivenRange(startDate,endDate,queryDate,Range.WEEK,holidayList,effortPerDay);
    System.out.println("EffortForTheRange:"+effortForTheGivenRange);
  }

  private static int getEffortFortheGivenRange(Calendar startDate,Calendar endDate,Calendar queryDate, Range range, List<Calendar> holidayList, int effortPerDay)
  {
    int effort = 0;
    if(!isWithinRange(startDate,endDate,queryDate))
    {
      throw new RuntimeException("QueryDate out of range");
    }
    
    if(range == Range.DAY)
    {
      if(isHoliday(queryDate, holidayList))
      {
        return 0;
      }
      else
      {
        return effortPerDay;
      }
    }
    else if(range == Range.WEEK)
    {
      queryDate = moveDateToTheStartOfTheWeek(queryDate);
      int i = 0;
      while(i < 7)
      {
        if(!isWithinRange(startDate, endDate, queryDate))
        {
          queryDate.add(Calendar.DATE, 1);
          i++;
          continue;
        }
        
        if(isHoliday(queryDate, holidayList))
        {
          queryDate.add(Calendar.DATE, 1);
        }
        else
        {
          effort = effort + effortPerDay;
          queryDate.add(Calendar.DATE, 1);
        }
        i++;
      }
    }
    
    //TODO
    return effort;
  }

  private static Calendar moveDateToTheStartOfTheWeek(Calendar queryDate) 
  {
    int dayOfTheWeek = queryDate.get(Calendar.DAY_OF_WEEK);
    int diff = dayOfTheWeek - 1;
    queryDate.add(Calendar.DATE, -diff);
    return queryDate;
  }

  private static boolean isWithinRange(Calendar startDate, Calendar endDate, Calendar queryDate) 
  {
    if(queryDate.after(endDate) || queryDate.before(startDate))
    {
      return true;
    }
    return false;
  }

  private static int calculateEffort(Calendar sd, Calendar ed, int effortHrs, List<Calendar> holidayList) 
  {
    int noOfAvailableDaysToWork = 0;
    while(sd.before(ed) || sd.equals(ed))
    {
      if(isHoliday(sd, holidayList))
      {
        sd.add(Calendar.DATE, 1);
        continue;
      }
      
      noOfAvailableDaysToWork++;
      sd.add(Calendar.DATE, 1);
    }
    return noOfAvailableDaysToWork;
  }

  private static boolean isHoliday(Calendar sd, List<Calendar> holidayList) {
    
    if(sd.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || sd.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
    {
      return true;
    }
    
    for (Calendar calendar : holidayList) 
    {
      if(sd.getTimeInMillis() == calendar.getTimeInMillis())
      {
        return true;
      }
    }
    return false;
  }
}
