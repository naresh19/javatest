package com.javatest.designpatterns.structural.decorator;
public class SimpleIcecream implements Icecream {

  @Override
  public String makeIcecream() {
    return "Base Icecream";
  }

}