package com.javatest.designpatterns.structural.decorator;
public interface Icecream {
  public String makeIcecream();
}