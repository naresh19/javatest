package com.javatest.designpatterns.creational.singleton;
public class Singleton {
	private static Singleton singleInstance = new Singleton();
	private Singleton() {}
	public static Singleton getSingleInstance() {
		return singleInstance;
	}
}