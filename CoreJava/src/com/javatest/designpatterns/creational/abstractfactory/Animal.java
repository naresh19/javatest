package com.javatest.designpatterns.creational.abstractfactory;

public interface Animal
{
    public void breathe();
}