package com.javatest.designpatterns.creational.abstractfactory;

public class SampleAbstractFactory
{

    public static AnimalFactory getAnimalFactory(String type)
    {
        if ("water".equals(type))
            return new SeaFactory();
        else
            return new LandFactory();
    }
}