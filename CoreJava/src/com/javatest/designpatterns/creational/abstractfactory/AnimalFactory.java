package com.javatest.designpatterns.creational.abstractfactory;

public interface AnimalFactory
{
    public Animal createAnimal();
}