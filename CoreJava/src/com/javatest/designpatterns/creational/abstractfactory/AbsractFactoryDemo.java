package com.javatest.designpatterns.creational.abstractfactory;

public class AbsractFactoryDemo
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        AnimalFactory animalFactory = SampleAbstractFactory.getAnimalFactory("water");
        Animal animal = animalFactory.createAnimal();
        animal.breathe();

    }

}
