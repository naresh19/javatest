package com.javatest.designpatterns.creational.abstractfactory;

public class SeaFactory implements AnimalFactory
{

    public Animal createAnimal()
    {
        return new Shark();
    }

}