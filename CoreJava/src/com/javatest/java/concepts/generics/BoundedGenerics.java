package com.javatest.java.concepts.generics;

public class BoundedGenerics
{

   class GenericNumberContainer <T extends Number> {
        private T obj;

        public GenericNumberContainer(){
        }
        
        public GenericNumberContainer(T t){
            obj = t;
        }
        /**
         * @return
     the obj
         */
        public T getObj() {
            return obj;
        }

        /**
         * @param obj the obj to set
         */
        public void setObj(T t) {
            obj = t;
        }
    }

    
    /**
     * @param args
     */
    public void test(String[] args)
    {
        GenericNumberContainer<Integer> gn = new GenericNumberContainer<Integer>();
        gn.setObj(3);

        // Type argument String is not within the upper bounds of type variable T
        //compilation error
        //GenericNumberContainer<String> gn2 = new GenericNumberContainer<String>();
    }
    
    
   
}
