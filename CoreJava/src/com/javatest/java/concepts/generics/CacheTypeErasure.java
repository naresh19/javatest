package com.javatest.java.concepts.generics;

public interface CacheTypeErasure<T>
{

    void put(T t);
}
