package com.javatest.java.concepts.generics;


//These are called as formal type parameters
//The formal type parameters will be replaced by actual types when the interface is implemented
public interface Cache<K , V>
{

    void put(K key,V value);
    
    V getValue(K key);
}
