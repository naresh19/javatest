package com.javatest.java.concepts.generics;

import java.util.ArrayList;
import java.util.List;


/**
 * Use of Wild Cards/Unbounded generics and also method generics
 *
 */
public class WildCards
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
     // Create List of type Integer
        List<Integer> intList = new ArrayList<Integer>();
        intList.add(2);
        intList.add(4);
        intList.add(6);

        // Create List of type String
        List<String> strList = new ArrayList<String>();
        strList.add("two");
        strList.add("four");
        strList.add("six");

        // Create List of type Object
        List<Object> objList = new ArrayList<Object>();
        objList.add("two");
        objList.add("four");
        objList.add(strList);

        checkList(intList, "test"); 
        // Output:  The list [2, 4, 6] does not contain the element: 3

        checkList(objList, strList); 
        /* Output:  The list [two, four, [two, four, six]] contains 
        the element: [two, four, six] */

        checkList(strList, objList);
        /* Output:  The list [two, four, six] does not contain 
        the element: [two, four, [two, four, six]] */

    }
    
    
    public static <N,T> void checkList(List<T> myList, N obj){
        if(myList.contains(obj)){
            System.out.println("The list "+myList + " contains the element: " + obj);
        } else {
            System.out.println("The list "+myList + " does not contain the element: " + obj);
        }
    }

}
