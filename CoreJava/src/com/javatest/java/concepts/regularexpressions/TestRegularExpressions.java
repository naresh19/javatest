package com.javatest.java.concepts.regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegularExpressions
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		String s = "Writing Fast Tests Against Enterprise Rails 60min";
		String s1 = "Writing Fast Tests Against Enterprise Rails lightning";
		Pattern p = Pattern.compile("(.*)(lightning|([0-9][0-9])min)");
		Matcher matcher = p.matcher(s);
		System.out.println(matcher.matches());
		String group = matcher.group(3);
		System.out.println(group);
	}

}
