package com.javatest.java.concepts.logicprograms;

public class TestFibonacciSeries
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		int maxLimit = 100;

		int fib1 = 0, fib2 = 1;

		System.out.println("Fibonacci Series:");

		System.out.print(fib1 + " ");

		int sum = 0;

		while (sum <= maxLimit)
		{
			sum = fib1+fib2;
			
			fib1 = fib2;
			
			fib2 = sum;
			
			System.out.print(sum + " ");
			
		}

	}
	
	
	//using recursion  
	public static int fibonacci(int number){
        if(number < 1){
            throw new IllegalArgumentException("Invalid argument for Fibonacci series: "
                                                + number);
        }
        //base case of recursion
        if(number == 1 || number == 2){
            return 1;
        }
        //recursive method call in java
        return fibonacci(number-2) + fibonacci(number -1);
    }
	
	/*
    * Java program to calculate Fibonacci number using loop or Iteration.
    * @return Fibonacci number
    */
   public static int fibonacci2(int number){
       if(number == 1 || number == 2){
           return 1;
       }
       int fibo1=1, fibo2=1, fibonacci=1;
       for(int i= 3; i<= number; i++){
           fibonacci = fibo1 + fibo2; //Fibonacci number is sum of previous two Fibonacci number
           fibo1 = fibo2;
           fibo2 = fibonacci;
         
       }
       return fibonacci; //Fibonacci number
     
   }   



}
