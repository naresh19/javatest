package com.javatest.java.concepts.logicprograms.recursion;

public class MultiplicationTable {

  public static void main(String[] args) {
    printMultiplicationTable(10,10);
  }

  private static void printMultiplicationTable(int i,int index) 
  {
    if(index == 0)
    {
      return;
    }
    printMultiplicationTable(i, index - 1);
    System.out.println(i + "*" + index + "=" + (i*index) + "\n");
  }


}
