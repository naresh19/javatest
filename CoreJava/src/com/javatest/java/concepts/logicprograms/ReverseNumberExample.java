package com.javatest.java.concepts.logicprograms;

import java.util.Scanner;

/**
 * Simple Java program to reverse a number in Java using loop and operator This
 * program also shows example of using division operator(/) and Remainder
 * Operator(%)
 */
public class ReverseNumberExample
{

    public static void main(String args[])
    {
        // input number to reverse
        System.out.println("Please enter number to be reversed using Java program: ");
        int number = new Scanner(System.in).nextInt();

        long reverse = reverse(number);
        System.out.println("Reverse of number: " + number + " is " + reverse);

    }

   

    public static long reverse(int number)
    {
        long reverse = 0;
        while (number != 0) {
            reverse = (reverse * 10) + number % 10;
            number /= 10;
        }
        return reverse;
    }

}
