package com.javatest.java.concepts.logicprograms.matrix;

public class EncryptString {

  public static void main(String[] args) {
    String input = "the black cat jumped on the roof";
    input = input.replaceAll(" ", "");
    System.out.println("input after removing spaces:" + input);
    int l = input.length();
    int rc[] = getRC(l);
    int r = rc[0];
    int c = rc[1];

    char[] ip = input.toCharArray();
    int charIndex = 0;
    char matrix[][] = new char[r][c];
    for (int i = 0; i < r; i++) {
      for (int j = 0; j < c; j++) {
        if (charIndex >= ip.length) {
          matrix[i][j] = ' ';
          continue;
        }
        matrix[i][j] = ip[charIndex];
        charIndex++;
      }
    }

    // printing matrix
    for (int column = 0; column < c; column++) {
      for (int row = 0; row < r; row++) {
        System.out.print(matrix[row][column]);
      }
      System.out.print(" ");
    }
  }

  private static int[] getRC(int l) {
    int rc[] = new int[] {0, 0};
    int r1 = (int) Math.sqrt(l);
    int c1 = r1;
    int sumOfrc1 = r1*c1;

    // row constant , increase column
    while (sumOfrc1 <= l) {
      if (sumOfrc1 >= l) {
        break;
      }
      c1++;
      sumOfrc1 =  (r1 * c1);
    }

    int r2 = (int) Math.sqrt(l);
    int c2 = r2;
    int sumOfrc2 = r2*c2;

    // row constant , increase column
    while (sumOfrc2 <= l) {
      if (sumOfrc2 >= l) {
        break;
      }
      r2++;
      sumOfrc2 =  (r2 * c2);
    }

    if ((r1 * c1) > (r2 * c2))
    {
      rc[0] = r2;
      rc[1] = c2;
    } 
    else 
    {
      rc[0] = r1;
      rc[1] = c1;
    }
    return rc;
  }

}
