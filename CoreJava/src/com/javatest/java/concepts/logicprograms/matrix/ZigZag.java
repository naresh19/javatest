package com.javatest.java.concepts.logicprograms.matrix;

import java.util.Scanner;

public class ZigZag {

  public static void main(String[] args) {

    int num_rows = 5, num_columns = 5;
    Scanner scanner = new Scanner(System.in);
//    System.out.println("Enter number of rows:");
//    num_rows = scanner.nextInt();
//    System.out.println("Enter number of columns:");
//    num_columns = scanner.nextInt();
//    int[][] matrix = new int[num_rows][num_columns];
//    System.out.println("Enter matrix data..");
//    for (int i = 0; i < num_rows; i++) {
//      for (int j = 0; j < num_columns; j++) {
//        matrix[i][j] = scanner.nextInt();
//      }
//    }
    
    int[][] matrix = new int[][] { {1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20}
    ,{21,22,23,24,25}};

    System.out.println("Entered matrix is below..");
    for (int i = 0; i < num_rows; i++) {
      for (int j = 0; j < num_columns; j++) {
        System.out.print(matrix[i][j] + "  ");
      }
      System.out.println();
    }

    boolean forward = true;
    // main logic goes here..
    for (int row = 0; row < num_rows; row++) {
      if (row % 2 == 0 && forward) {
        for (int column = 0; column < num_columns; column++) {
          System.out.print(matrix[row][column] + " ");
        }
        forward = false;
      } else if (row % 2 == 0 && !forward) {
        for (int column = num_columns-1; column>=0; column--) {
          System.out.print(matrix[row][column] + " ");
        }
        forward = true;
      }
      if (row % 2 != 0 && !forward) {
        System.out.print(matrix[row][num_columns-1] + " ");
      } else if (row % 2 != 0 && forward) {
        System.out.print(matrix[row][0] + " ");
      }


    }

  }

}
