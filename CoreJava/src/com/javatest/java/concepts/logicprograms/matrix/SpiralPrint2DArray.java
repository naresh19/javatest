package com.javatest.java.concepts.logicprograms.matrix;
public class SpiralPrint2DArray {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[][] A = { { 1,  2,  3,  4 }, 
					  { 12, 13, 14, 5 }, 
					  { 11, 16, 15, 6 },
					  { 10,  9,  8, 7 } };
		int[][] A1 = { { 1,  12,  3,  4 }, 
				       { 2,  13, 14, 15 }, 
				       { 7,   9,  5, 6 },
				       { 10,  16,  8, 11 } };
		
		System.out.println(A.length);
		System.out.println(A[0].length);
		printSpiral(A); //Prints 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 
		System.out.println();
		printSpiral(A1);//1, 12, 3, 4, 15, 6, 11, 8, 16, 10, 7, 2, 13, 14, 5, 9, 

	}
	
	public static void printSpiral(int[][] A) {
		int rowStart = 0;
		int colStart = 0;
		int rowEnd = A.length;
		int colEnd = A[0].length;
		do {
			for (int i = colStart; i < rowEnd; i++) {
				System.out.print(A[rowStart][i] + ", ");
			}
			// current row is done and no more need to process it
			rowStart++;
			for (int i = rowStart; i < colEnd; i++) {
				System.out.print(A[i][colEnd - 1] + ", ");
			}
			// current column is done and no more need to process it
			colEnd--;

			if (rowStart < rowEnd) {
				for (int i = colEnd - 1; i >= colStart; i--) {
					System.out.print(A[rowEnd - 1][i] + ", ");
				}
				rowEnd--;
			}
			if (colStart < colEnd) {
				for (int i = rowEnd - 1; i >= rowStart; i--) {
					System.out.print(A[i][colStart] + ", ");
				}
				colStart++;
			}

		} while (rowStart < rowEnd && colStart < colEnd);
	}

}