package com.javatest.java.concepts.logicprograms.matrix;

public class SpiralMatrix {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    //equal m=n
    int matrix[][] = new int[][] {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15},
        {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        
    // m!=n
    matrix = new int[][] {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15},
          {16, 17, 18, 19, 20}};
          
    //3*4
    matrix = new int[][] {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
          
    int rowsCount = matrix.length;
    int colCount = matrix[0].length;

    int rowStart = 0;
    int colStart = 0;

    while (rowsCount >= 0 && colCount >= 0) {
      // do printing in 4 directions

      // direction - left to right
      // row constant , col changing
      for (int j = colStart; j < colCount; j++) {
        System.out.print(matrix[rowStart][j] + " ");
      }
      
      //direction - extreme top right to down
      // column constant, row changing
      for (int j = rowStart+1; j < rowsCount; j++) {
        System.out.print(matrix[j][colCount-1] + " ");
      }
      
      //direction - extreme bottom right to left
      //row constant, column changing
      for (int j = colCount-2; j>= colStart; j--) {
        System.out.print(matrix[rowsCount-1][j] + " ");
      }
      
      //direction - bottom left to top
      // column constant, row changing
      for (int j = rowsCount-2; j > rowStart; j--) {
        System.out.print(matrix[j][colStart] + " ");
      }
      
      System.out.println();
      
      //
      rowStart++;
      colStart++;

      rowsCount = rowsCount - 1;
      colCount = colCount - 1;

    }

  }

}
