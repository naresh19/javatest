package com.javatest.java.concepts.logicprograms;

import java.util.ArrayList;

public class PermutationsOfString
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		ArrayList<String> permutations = permutationsOf("naresh");
        System.out.println("final:"+permutations);
	}

	public static ArrayList<String> permutationsOf(String s)
	{
		ArrayList<String> result = new ArrayList<String>();
		if (s.length() == 1)
		{ // base case
			// return a new ArrayList containing just s
			result.add(s);
			return result;
		}
		else
		{
			// separate the first character from the rest
			char first = s.charAt(0);
			String rest = s.substring(1);
			// get all permutationsOf the rest of the characters
			ArrayList<String> simpler = permutationsOf(rest); // recursive step
			// for each permutation,
			for (String permutation : simpler)
			{ // extra work // add the first character in all possible
				// positions, and
				ArrayList additions = insertAtAllPositions(first, permutation);
				// put each result into a new Arraylist
				result.addAll(additions);
			}
			return result;
		}

	}

	private static ArrayList<String> insertAtAllPositions(char ch, String s)
	{
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i <= s.length(); i++)
		{
			String inserted = s.substring(0, i) + ch + s.substring(i);
			result.add(inserted);
		}
		return result;
	}

}
