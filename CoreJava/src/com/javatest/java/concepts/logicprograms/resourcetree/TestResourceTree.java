package com.javatest.java.concepts.logicprograms.resourcetree;

import java.util.List;

public class TestResourceTree {

  private static ResourceRepository repository = new ResourceRepository();

  public static void main(String[] args) {
    // TODO Auto-generated method stub

    // Let's create some test data
    // Resource rootResource = new Resource(1,"RootResource",null);

    Resource rootRes1 = new Resource(1, "R1", null);
    repository.addResource(rootRes1);
    repository.addResource(new Resource(2, "R2", null));
    repository.addResource(new Resource(3, "R11", 1));
    repository.addResource(new Resource(4, "R12", 1));
    repository.addResource(new Resource(5, "R21", 2));
    repository.addResource(new Resource(6, "R22", 2));
    repository.addResource(new Resource(7, "R111", 3));
    repository.addResource(new Resource(8, "R112", 3));
    repository.addResource(new Resource(9, "R121", 4));
    repository.addResource(new Resource(10, "R122", 4));
    repository.addResource(new Resource(11, "R211", 5));
    repository.addResource(new Resource(12, "R212", 5));
    repository.addResource(new Resource(13, "R221", 6));
    repository.addResource(new Resource(14, "R222", 6));

    printBatchResources(rootRes1);

    System.out.println("Test Done");
  }

  private static void printBatchResources(Resource rootRes1) {
    List<Resource> childResources = repository.getChildResources(rootRes1.getResourceId());
    if (null != childResources) {
      for (Resource resource : childResources) {
        printBatchResources(resource);
        System.out.println("Deleting.."+resource);
      }
    }
  }

}
