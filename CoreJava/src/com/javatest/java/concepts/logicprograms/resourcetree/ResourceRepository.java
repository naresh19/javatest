package com.javatest.java.concepts.logicprograms.resourcetree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceRepository {

  private Map<Integer, Resource> resources;

  private Map<Integer, List<Resource>> childResources;

  public ResourceRepository() {

    resources = new HashMap<Integer, Resource>();
    childResources = new HashMap<Integer, List<Resource>>();
  }

  public Resource getResource(Integer resourceId) {
    return resources.get(resourceId);
  }

  public List<Resource> getChildResources(Integer id) {
    return childResources.get(id);
  }

  public void addResource(Resource resource) {
    resources.put(resource.getResourceId(), resource);
    if (resource.getParentId() != null) {
      Integer parentId = resource.getParentId();
      if (childResources.get(parentId) == null) {
        childResources.put(parentId, new ArrayList<Resource>());
      }
      List<Resource> list = childResources.get(parentId);
      list.add(resource);
    }
  }

  public List<Resource> getAllResources() {
    return (List<Resource>) resources.values();
  }
  
 }
