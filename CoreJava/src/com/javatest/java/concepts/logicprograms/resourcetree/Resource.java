package com.javatest.java.concepts.logicprograms.resourcetree;

public class Resource {
  private String resourceName;

  private Integer resourceId;

  private Integer parentId;

  public Resource(Integer resourceId, String resourceName, Integer parentResourceId) {

    this.resourceId = resourceId;
    this.resourceName = resourceName;
    this.parentId = parentResourceId;
  }

  public String getResourceName() {
    return resourceName;
  }


  public Integer getResourceId() {
    return resourceId;
  }


  public Integer getParentId() {
    return parentId;
  }

  @Override
  public String toString() {
    return "Resource Id:" + resourceId + ",Resource Name:" + resourceName;
  }


}
