package com.javatest.java.concepts.logicprograms.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrimeNumber
{

    /**
     * @param args
     * @throws IOException 
     * @throws NumberFormatException 
     */
    public static void main(String[] args) throws NumberFormatException, IOException
    {
        PrimeNumber pn = new PrimeNumber();
        BufferedReader kb = new
        BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter n: ");
        int n = Integer.parseInt(kb.readLine());
        pn.primes(n);

    }

    public void primes(int n)
    {
        int k, m;
        System.out.print("Prime numbers up to " + n + ": 2 3");
        for (m = 3; m <= n; m = m + 2) {
            boolean isPrime = false;
            for (k = 2; k <= m / 2; k++) {
                if (m % k == 0) {
                    isPrime = false;
                    break;
                } else
                    isPrime = true;
            }
            if (isPrime)
                System.out.print(" " + m);
        }
    }

}
