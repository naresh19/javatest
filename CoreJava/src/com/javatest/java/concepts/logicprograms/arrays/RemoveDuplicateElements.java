package com.javatest.java.concepts.logicprograms.arrays;
public class RemoveDuplicateElements {
 
    // Note that first the array needs to be sorted.
    public static int[] removeDuplicates(int[] input){
         
        int i = 0;
        int j = 1;
        //return if the array length is less than 2
        if(input.length < 2){
            return input;
        }
        while(j < input.length){
            if(input[j] == input[i]){
                j++;
            }else{
                input[++i] = input[j++];
            }    
        }
        int[] output = new int[i+1];
        for(int k=0; k<output.length; k++){
            output[k] = input[k];
        }
         
        return output;
    }
     
    public static void main(String a[]){
        int[] input1 = {2,2,2,2,3,6,6,8,9,10,10,10,12,12};
        int[] output = removeDuplicates(input1);
        for(int i:output){
            System.out.print(i+" ");
        }
    }
}