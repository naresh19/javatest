package com.javatest.java.concepts.logicprograms.arrays;

import java.util.ArrayList;
import java.util.List;

public class SubsetsOfArray
{

	// take each number i from 0 to 2n and check each bit. If bit k from number
	// i
	// is 1, then consider arrayk to be in the current subset
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		int array[] = new int[]
		{ 1,2,3,4};
		subsets_iterative(array, 4);
	}

	private static void subsets_iterative(int[] vec, int size)
	{
	    //STEP 1: find the number of subsets possible.
	    // 2 power n susbsets are possible for an array of size n
		double n = Math.pow((double) 2, (double) size);
		
		List<Integer> currentSubset = new ArrayList<Integer>(2);
		for (int i = 0; i < n; i++)
		{
			currentSubset.clear();
			for (int j = 0; j < size; j++)
			{
			    // check if bit j in the number i is on or off ?
			    // for example in number 2, i.e, 0010 , add the 3rd element in array to list
				if (((i & (1 << j)) != 0))
				//if (((i >> j) & 1) != 0)
				{
					currentSubset.add(vec[j]);
				}
			}
			System.out.println(currentSubset);
		}
	}
}
