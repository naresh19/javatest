package com.javatest.java.concepts.logicprograms.arrays;

import java.util.Arrays;

class BinarySearchDemo
{
    static Integer[] a = {
        23,56,78,28,54,75,13
    };
    static Integer key = 54;

    public static void main(String args[])
    {
        //first sort the array
        Arrays.sort(a);
        
        if (binarySearch())
            System.out.println(key + " found in the list");
        else
            System.out.println(key + " not found in the list");
    }

    static boolean binarySearch()
    {
        int c, mid, low = 0, high = a.length - 1;
        while (low <= high) {
            mid = (low + high) / 2;
            c = key.compareTo(a[mid]);
            if (c < 0)
                high = mid - 1;
            else if (c > 0)
                low = mid + 1;
            else
                return true;
        }
        return false;
    }
}