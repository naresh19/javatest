package com.javatest.java.concepts.logicprograms.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// http://javabynataraj.blogspot.com

public class MatrixMultiplication
{
    public static void main(String[] args)
        throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of the matrixA rows(m) & columns(n) :");
        int m = Integer.parseInt(br.readLine()); // matrixA row size 'm'
        int n = Integer.parseInt(br.readLine()); // matrixA column size 'n'
        System.out.println("Enter the size of the matrixB rows(p) & columns(q) :");
        int p = Integer.parseInt(br.readLine()); // matrixB row size 'p'
        int q = Integer.parseInt(br.readLine()); // matrixB column size 'q'

        int[][] matrixA = new int[m][n];
        int[][] matrixB = new int[p][q];

        if (n != p) {
            throw new IllegalArgumentException("matrixA columns(n) and matrixB rows(p) should be equal: " + n + " != " + p);
        }
        int[][] matrixC = new int[m][q]; // create resultant matrixC with size
                                         // mxq
        System.out.println("Enter matrixA values: (press enter-key for each value)");
        for (int i = 0; i < m; i++) { // Read the values for matrixA
            for (int j = 0; j < n; j++) {
                matrixA[i][j] = Integer.parseInt(br.readLine());
            }
        }

        System.out.println("Enter matrixB values: (press enter-key for each value)");
        for (int i = 0; i < p; i++) { // Read the values for matrixB
            for (int j = 0; j < q; j++) {
                matrixB[i][j] = Integer.parseInt(br.readLine());
            }
        }

        System.out.println("Entered matrixA[" + m + "x" + n + "] is: ");
        System.out.println("---------------------------");
        for (int i = 0; i < m; i++) { // display matrixA
            for (int j = 0; j < n; j++) {
                System.out.print(matrixA[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Entered matrixB[" + p + "x" + q + "] is: ");
        System.out.println("---------------------------");
        for (int i = 0; i < p; i++) { // display matrixB
            for (int j = 0; j < q; j++) {
                System.out.print(matrixB[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Resultant matrixC[" + m + "x" + q + "] is: ");
        System.out.println("---------------------------");
        for (int i = 0; i < m; i++) { // matrix multiplication
            for (int j = 0; j < q; j++) {
                for (int k = 0; k < n; k++) {
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }
                System.out.print(matrixC[i][j] + " ");
            }
            System.out.println();
        }
    }
}

// Read more:
// http://javabynataraj.blogspot.com/2014/08/matrix-multiplication-using-arrays-in.html#ixzz3A9rtalaA
