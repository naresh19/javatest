package com.javatest.java.concepts.logicprograms.arrays;

import java.util.Arrays;

public class ReverseArray
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        int[] integers = new int[]{1,2,3,4,5};
        reverse(integers);
        
        System.out.println(Arrays.toString(integers));
    }

    public static void reverse(int[] a)
    {
        for (int i = 0; i < a.length / 2; i++) {
            int temp = a[i]; // swap using temporary storage
            a[i] = a[a.length - i - 1];
            a[a.length - i - 1] = temp;
        }
    }

}
