package com.javatest.java.concepts.logicprograms.arrays;

public class SecondHighest
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        int array[] = {
            19, 1, 5, 3, 7, 9,18
        };

        int first = -1;
        int second = -1;


        for (int i = 0; i < array.length; i++) {

            if (array[i] > first) {
                int tmp = first;
                first = array[i];
                second = tmp;
            } else if (array[i] > second) {
                second = array[i];
            }
        }

        System.out.println("Second Largest is:" + second);
    }

}
