package com.javatest.java.concepts.logicprograms.arrays;


public class MaxSumOfContiguousSubArray
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        int a[] = {-2,1,6,-1,7,2,5,-21,21,25};
        findMaxSumSequence(a, a.length);

    }
    
    private static void findMaxSumSequence (int inputArray[], int size)
    {
      if (size == 0)
        throw new RuntimeException("Array Size is 0");

      int maxSum = inputArray[0];
      int maxStartIndex = 0;
      int maxEndIndex = 0;
      
      int curSum = inputArray[0];
      int curStartIndex = 0;
      

      for (int i = 1; i < size; i++)
      {
        if (curSum < 0)
        {
          curSum = 0;
          curStartIndex = i;
        }
        
        curSum = curSum + inputArray[i];

        if (curSum > maxSum)
        {

          maxSum = curSum;
          maxStartIndex = curStartIndex;
          maxEndIndex = i;
        }
      } 

      System.out.println("Start index: " + maxStartIndex + " End index: " 
            + maxEndIndex + " Max sum: " + maxSum );
    }

}
