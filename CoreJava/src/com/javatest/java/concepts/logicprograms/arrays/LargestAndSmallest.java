package com.javatest.java.concepts.logicprograms.arrays;

public final class LargestAndSmallest
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        int array[] = {
            19, 1, 5, 3, 7, 9, 18
        };

        int max = array[0];
        int min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            } else if (array[i] < min) {
                min = array[i];
            }

        }

        System.out.println("First:" + max + ",Second:" + min);
    }
    
    //try solving this issue using divide & conquer

}
