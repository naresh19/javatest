package com.javatest.java.concepts.serialization;

//though this class is not serializable, subclasses can still be serialized.
//but not that default constructor is mandatory in this class since it is
//required during deserialization process. 
public class Dog
{

   protected Bird t = new Bird();
   protected int b ;
   
   public Dog(int count)
   {
     b = count;
   }
   
   public Dog()
   {
       //during deserialization b will be initialized to 10
       b = 10;
   }
   
   public int getC()
   {
       return b;
   }
}
