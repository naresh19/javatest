package com.javatest.java.concepts.serialization;

import java.io.Serializable;

public class Cat  extends Dog implements Serializable
{

    
    public int k = 20;
    
    public Cat(int i)
    {
        super(3);
    }

    //If u uncomment this, serilization will fail because Dog class is not serializable
    //private Dog d = new Dog();
	/**
	 * 
	 */
	private static final long serialVersionUID = -6628157388289520054L;

}
