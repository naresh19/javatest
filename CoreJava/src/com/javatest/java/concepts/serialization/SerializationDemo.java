package com.javatest.java.concepts.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationDemo
{

	public static void main(String[] args)
	{
		Cat c = new Cat(3); // 2
		try
		{
			FileOutputStream fs = new FileOutputStream("testSer.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(c); // 3
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			FileInputStream fis = new FileInputStream("testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			c = (Cat) ois.readObject(); // 4
			System.out.println(c.k);
			System.out.println(c.getC());
			ois.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
