package com.javatest.java.concepts.multithreading.approach2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class EvenOddGenTest
{

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException
	{

		NumberGenerator numGenerator = new NumberGenerator();

		OddGenerator oddGen = new OddGenerator(numGenerator);
		EvenGenerator evenGen = new EvenGenerator(numGenerator);

		// oddGen.start();
		// evenGen.start();

		ExecutorService executorService = new ThreadPoolExecutor(10, 20, 10,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(20));
		executorService.submit(oddGen);
		executorService.submit(evenGen);
		
	}

}