package com.javatest.java.concepts.multithreading.approach1;

public class EvenThread implements Runnable
{

    private LockObject lockObject;

    public EvenThread(LockObject object)
    {
        this.lockObject = object;
    }

    public void run()
    {
        for (int i = 2; i < 20; i += 2) {
            synchronized (lockObject) {
                while (lockObject.isOddTurn()) {
                    try {
                        lockObject.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Even Thread:" + i);
                lockObject.setOddTurn(true);
                lockObject.notify();
            }
        }

        System.out.println("Even Thread Execution Done!!");

    }

}
