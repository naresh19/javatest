package com.javatest.java.concepts.multithreading.approach1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class EvenOddThreadTest
{

    /**
     * @param args
     * @throws InterruptedException
     * @throws ExecutionException 
     */
    public static void main(String[] args)
        throws InterruptedException, ExecutionException
    {

        LockObject object = new LockObject();

//        Thread evenThread = new Thread(new EvenThread(object), "EVEN_THREAD");
//        Thread oddThread = new Thread(new OddThread(object), "ODD_THREAD");
//
//        evenThread.start();
//        oddThread.start();
//
//        evenThread.join();
//        oddThread.join();
        
        ExecutorService executorService = new ThreadPoolExecutor(10, 20, 10,
            TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(20));
        Future<? > submit = executorService.submit(new OddThread(object));
        Future<? > submit2 = executorService.submit(new EvenThread(object));

        System.out.println("Main Thread Execution Done!");

    }

}
