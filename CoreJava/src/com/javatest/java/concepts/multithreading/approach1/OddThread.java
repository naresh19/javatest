package com.javatest.java.concepts.multithreading.approach1;

public class OddThread implements Runnable
{

    private LockObject lockObject;

    public OddThread(LockObject object)
    {
       this.lockObject = object;
    }

    public void run()
    {
        for (int i = 1; i < 20; i += 2) {
            synchronized (lockObject) {
                while (!lockObject.isOddTurn()) {
                    try {
                        lockObject.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Odd Thread:" + i);
                lockObject.setOddTurn(false);
                lockObject.notify();
            }
        }

        System.out.println("Odd Thread Execution Done!!");
        
    }

}
