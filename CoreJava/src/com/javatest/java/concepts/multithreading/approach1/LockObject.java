package com.javatest.java.concepts.multithreading.approach1;

public class LockObject
{

    private volatile boolean isOddTurn = true;

    public boolean isOddTurn()
    {
        return isOddTurn;
    }

    public void setOddTurn(boolean isOddTurn)
    {
        this.isOddTurn = isOddTurn;
    }
}
