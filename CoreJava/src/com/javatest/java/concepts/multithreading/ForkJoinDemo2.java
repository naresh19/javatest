package com.javatest.java.concepts.multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/*
 * The Fork/Join Framework in Java 7 is designed for work that can be broken down into smaller tasks
 * and the results of those tasks combined to produce the final result. In general, classes that use
 * the Fork/Join Framework follow the following simple algorithm:
 * 
 * // pseudocode Result solve(Problem problem) { if (problem.size < SEQUENTIAL_THRESHOLD) return
 * solveSequentially(problem); else { Result left, right; INVOKE-IN-PARALLEL { left =
 * solve(extractLeftHalf(problem)); right = solve(extractRightHalf(problem)); } return combine(left,
 * right); } } In order to demonstrate this, I have created an example to find the maximum number
 * from a large array using fork/join:
 */
public class ForkJoinDemo2 extends RecursiveTask<Integer> {

  private static final int SEQUENTIAL_THRESHOLD = 5;

  private final int[] data;
  private final int start;
  private final int end;

  public ForkJoinDemo2(int[] data, int start, int end) {
    this.data = data;
    this.start = start;
    this.end = end;
  }

  public ForkJoinDemo2(int[] data) {
    this(data, 0, data.length);
  }

  @Override
  protected Integer compute() {
    final int length = end - start;
    if (length < SEQUENTIAL_THRESHOLD) {
      return computeDirectly();
    }
    OptionalInt max =
        ForkJoinTask.invokeAll(createSubtasks(length)).stream().mapToInt(d -> d.join()).max();
    return max.getAsInt();


    // final int split = length / 2;
    // final ForkJoinDemo2 left = new ForkJoinDemo2(data, start, start + split);
    // left.fork();
    // final ForkJoinDemo2 right = new ForkJoinDemo2(data, start + split, end);
    // return Math.max(right.compute(), left.join());
  }

  private List<ForkJoinDemo2> createSubtasks(int length) {
    List<ForkJoinDemo2> subtasks = new ArrayList<>();

    // String partOne = workload.substring(0, workload.length() / 2);
    // String partTwo = workload.substring(workload.length() / 2, workload.length());

    final int split = length / 2;
    subtasks.add(new ForkJoinDemo2(data, start, start + split));
    subtasks.add(new ForkJoinDemo2(data, start + split, end));

    return subtasks;
  }

  private Integer computeDirectly() {
    System.out.println(Thread.currentThread() + " computing: " + start + " to " + end);
    int max = Integer.MIN_VALUE;
    for (int i = start; i < end; i++) {
      if (data[i] > max) {
        max = data[i];
      }
    }
    return max;
  }

  public static void main(String[] args) {
    // create a random data set
    final int[] data = new int[1000];
    final Random random = new Random();
    for (int i = 0; i < data.length; i++) {
      data[i] = random.nextInt(100);
    }

    // submit the task to the pool
    final ForkJoinPool pool = new ForkJoinPool(4);
    final ForkJoinDemo2 finder = new ForkJoinDemo2(data);
    System.out.println(pool.invoke(finder));
  }
}
