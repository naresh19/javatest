package com.javatest.java.concepts.multithreading.prodconsprob;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumerProblemSolution
{

    /**
     * @param args
     */

    private static final int MAX_QUEUE_SIZE = 10;
    private static List<Integer> sharedQueue = new ArrayList<Integer>(10);

    public static void main(String[] args)
    {

        Thread producerThread = new Thread(new Producer(sharedQueue));
        Thread consumerThread = new Thread(new Consumer(sharedQueue));

        System.out.println("PRODUCER-CONSUMER PROCESS STARTED!!");
        producerThread.start();
        consumerThread.start();
    }

    public static class Producer implements Runnable
    {

        private List<Integer> sharedQueue;

        public Producer(List<Integer> sharedQ)
        {
            this.sharedQueue = sharedQ;
        }

        public void run()
        {
            int i = 0;

            while (i < 50) {
                synchronized (sharedQueue) {

                    while (sharedQueue.size() == MAX_QUEUE_SIZE) {
                        try {
                            sharedQueue.wait();
                            System.out.println("Q is full...Producer Waiting...");
                        } catch (InterruptedException e) {
                            System.out.println(e);
                        }
                    }

                    System.out.println("Produced:" + i);
                    sharedQueue.add(i++);
                    sharedQueue.notifyAll();

                }
            }

            System.out.println("PRODUCER THREAD DONE!!");

        }

    }

    public static class Consumer implements Runnable
    {

        private List<Integer> sharedQueue;

        public Consumer(List<Integer> sharedQ)
        {
            this.sharedQueue = sharedQ;
        }

        public void run()
        {
            while (true) {
                
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                synchronized (sharedQueue) {

                    while (sharedQueue.size() == 0) {
                        try {
                            sharedQueue.wait();
                            System.out.println("Q is empty...consumer Waiting...");
                        } catch (InterruptedException e) {
                            System.out.println(e);
                        }
                    }

                    System.out.println("Consumed:" + sharedQueue.remove(0));
                    sharedQueue.notifyAll();

                }
            }

        }

    }

}
