package com.javatest.java.concepts.core;

public class TestInit
{

	private static int  i = getInt();
	
	static
	{
		System.out.println("In Static"+i);
	}
	
	{
		System.out.println("In blocks....");
	}
	public TestInit()
	{
		System.out.println("In Constructor"+i);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new TestInit();
		System.out.println("In Main...");

	}
	private static int getInt()
	{
		System.out.println("getInt");
		return 1;
	}

}
