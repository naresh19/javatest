package com.javatest.java.concepts.core;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class IteratorExample{

    public static void main(String[] args) {
        //HashMap instance used for Iterator Example in Java 
        HashMap<Integer, String> stockTable=new HashMap<Integer,String>();

        //Populating HashMap instance with sample values
        stockTable.put(new Integer(1), "Two");
        stockTable.put(new Integer(2), "One");
        stockTable.put(new Integer(4), "Four");
        stockTable.put(new Integer(3), "Three");

        //Getting Set of keys for Iteration using entry set
        Set<Entry<Integer, String>> stockSet = stockTable.entrySet();

        // Using Iterator to loop Map in Java, here Map implementation is HashMap
        Iterator<Entry<Integer, String>> i= stockSet.iterator();
        System.out.println("Iterating over HashMap in Java");
        
        //Iterator begins
        while(i.hasNext()){
            Map.Entry<Integer,String> m=i.next();
            int key = m.getKey();
            String value=m.getValue();
            System.out.println("Key :"+key+"  value :"+value);

        }
        
        
        //using for each
        for (Entry<Integer, String> entry : stockSet) {
            int key = entry.getKey();
            String value=entry.getValue();
            System.out.println("Key :"+key+"  value :"+value);
        }
        
        //key set
        Set<Integer> keys = stockTable.keySet();
        Collection<String> values = stockTable.values();
        
        
        System.out.println("Iteration over HashMap finished");
    }
}

