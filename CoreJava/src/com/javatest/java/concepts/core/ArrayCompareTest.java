package com.javatest.java.concepts.core;
import java.util.Arrays;

/**
 *
 * Java program to compare two Arrays in Java to see if they are equal or not.
 * We will example of comparing both primitive array e.g. int, long or double array
 * and Object array e.g. String array to see if they are equal or not.
 *
 * @author http://javarevisited.blogspot.com
 */
public class ArrayCompareTest {

    public static void main(String args[]) {
      
       //comparing primitive int arrays in Java
        int[] i1 = new int[] {1,2,3,4};
        int[] i2 = new int[] {1,2,3,4};
        int[] i3 = new int[] {0,2,3,4};
      
        //Arrays.equals() compare Array and return true if both array are equal
        //i..e either both of them are null or they are identical in length, and each pair
        //match each other e.g. i[0]=i2[0], i[1]=i2[1] and so on
      
        //i1 and i2 should be equal as both contains same elements
        boolean result = Arrays.equals(i1, i2);
        System.out.println("Comparing int array i1: " + Arrays.toString(i1)
                            + " and i1: " + Arrays.toString(i2));
        System.out.println("Does array i1 and i2 are equal : " + result);
      
        //array ii2 and i3 are not equals as only length is same, first pair is not same
        result = Arrays.equals(i2, i3);
        System.out.println("Comparing int array i2: " + Arrays.toString(i2)
                            + " and i3: " + Arrays.toString(i3));
        System.out.println("Does array i2 and i3 are equal : " + result);
      
        //comparing floating point or double arrays in Java
        double[] d1 = new double[] {1.5, 2.4, 3.2, 4,1};
        double[] d2 = new double[] {1.5, 2.4, 3.2, 4,1};
        double[] d3 = new double[] {0.0, 2.4, 3.2, 4,1};
      
        //Comparing two floating point arrays using Arrays.equals() in Java
      
        //double array d1 and d2 should be equal - length same, each index matches
        result = Arrays.equals(d1, d2);
        System.out.println("Comparing double array d1: " + Arrays.toString(d1)
                            + " and d2: " + Arrays.toString(d2));
        System.out.println("Does double array d1 and d2 are equal : " + result);
      
        //double array d2 and d3 is not equal - length same, first pair does not match
        result = Arrays.equals(d2, d3);
        System.out.println("Comparing double array d2: " + Arrays.toString(d2)
                            + " and d3: " + Arrays.toString(d3));
        System.out.println("Does double array d2 and d3 are same : " + result);
      
        //comparing Object array, here we will use String array
        String[] s1 = new String[]{"One", "Two", "Three"};
        String[] s2 = new String[]{"One", "Two", "Three"};
        String[] s3 = new String[]{"zero", "Two", "Three"};
      
        //String array s1 and s2 is equal - length same, each pair matches
        result = Arrays.equals(s1, s2);
        System.out.println("Comparing two String array s1: " + Arrays.toString(s1)
                            + " and s2: " + Arrays.toString(s2));

        System.out.println("Are both String array s1 and s2 are equal : " + result);
      
        //String array s2 and s3 is not equal - length same, first pair different
        result = Arrays.equals(d2, d3);
        System.out.println("Comparing two String array s2: " + Arrays.toString(s2)
                             + " and s3: " + Arrays.toString(s3));

        System.out.println("Are both String array s2 and s3 are equal : " + result);
      
        //Comparing nested arrays with equals and deepEquals method
        //Arrays.equals() method does not compare recursively,
        //while deepEquals() compare recursively
        //if any element inside Array is type of Array itself,
        //as here second element is String array
       
        Object[] o1 = new Object[]{"one", new String[]{"two"}};
        Object[] o2 = new Object[]{"one", new String[]{"two"}};
      
        System.out.println("Object array o1: " + Arrays.toString(o1) + " and o2: "
                            + Arrays.toString(o2));
        System.out.println("Comparing Object Array o1 and o2 with Arrays.equals : "
                            + Arrays.equals(o1, o2));
        System.out.println("Comparing Object Array o1 and o2 with Arrays.deepEquals : "
                            + Arrays.deepEquals(o1, o2));
    } 
  
}


