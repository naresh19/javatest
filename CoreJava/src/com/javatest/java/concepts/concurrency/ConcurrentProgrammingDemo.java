package com.javatest.java.concepts.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ConcurrentProgrammingDemo
{

	public static void main(String[] args) throws InterruptedException,
			ExecutionException
	{
		WorkerThread thread1 = new WorkerThread(1);
		WorkerThread thread2 = new WorkerThread(2);

		ExecutorService executorService = Executors.newFixedThreadPool(3);
		
		//for scheduling tasks
		ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
		service.scheduleAtFixedRate(new Runnable(){

            @Override
            public void run()
            {
                // TODO Auto-generated method stub
                
            }
		    
		}, 1, 1, TimeUnit.MINUTES);
		
		List<Callable<Integer>> list = new ArrayList<Callable<Integer>>(2);
		list.add(thread1);
		list.add(thread2);

//		Future<Integer> submit = executorService.submit(thread1);
//		Integer integer = submit.get();
//		System.out.println(integer);
		
		List<Future<Integer>> invokeAll = executorService.invokeAll(list);
		for (Future<Integer> future : invokeAll)
		{
			System.out.println(future.get());
		}

	}
}

class WorkerThread implements Callable<Integer>
{

	private int data;

	public WorkerThread(int i)
	{
		this.data = i;
	}

	public Integer call() throws Exception
	{
		data += 1;
		return data;
	}

}
