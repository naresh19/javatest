package com.javatest.java.concepts.file;

/*
 * Write a program that prints last n lines of a file like Unix tail command. Program
 * should take file name and number of lines to print from command line arguments. Assume
 * that each line is of 100 bytes approx.
 */
/*http://www.informit.com/guides/content.aspx?g=java&seqNum=226*/

import java.io.*;

class TailCommand
{
	public static void main(String args[])
	{
		try
		{
		    //you can open a random access file in either read/write mode
			RandomAccessFile randomFile = new RandomAccessFile(args[0], "r");
			long numberOfLines = Long.valueOf(args[1]).longValue();

			long fileLength = randomFile.length();

			// guessing 100 bytes per line !
			long startPosition = fileLength - (numberOfLines * 100);

			if (startPosition < 0)
				startPosition = 0;

			randomFile.seek(startPosition);

			String line = "";
			while ((line = randomFile.readLine()) != null)
			{
				System.out.println(line);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}