package com.javatest.java.concepts.java8.streams;

public class CollectorInterface
{

	//TODO
	// need to practice more on creating custom collectors
	
	// mainly u need to implement 4 methods
	// Supplier, BiConsumer, Function , BinaryOperator
	
	// Supplier -> initialize the collection in which the data is to be cvollected
	
	// BiConsumer -> The accumulator impl goes here
	
	// Function -> the logic for collecting into the final collection
	// mostly it will be Function.identity() since accumulator itself may contain
	// the final result
	
	// BinaryOperator -> this is combiner used for paralle operations
	// contains the logic of merging the accumulators
}
