package com.javatest.java.concepts.java8.streams;

import static java.util.stream.Collectors.toList;

import java.util.List;

import com.javatest.java.concepts.java8.streams.beans.Dish;

public class FilteringAndSlicing
{

	public static void main(String[] args)
	{
		List<Dish> menu = Dish.menu;
		// menu.stream().filter((Dish d )-> d.isVegetarian());
		//Filtering with a predicate
		List<Dish> collect = menu.stream().filter(Dish::isVegetarian).collect(toList());
		
		//Filtering Unique Elements
		menu.stream().filter(d -> d.getCalories() > 300).distinct().forEach(System.out::println);;
		
		//Truncating a stream
		menu.stream().filter(d -> d.getCalories() > 300).limit(3).forEach(System.out::println);
		
		//skipping elements
		menu.stream().filter(d -> d.getCalories() > 300).skip(3).forEach(System.out::println);
	}

}
