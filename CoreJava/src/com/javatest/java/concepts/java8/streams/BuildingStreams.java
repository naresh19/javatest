package com.javatest.java.concepts.java8.streams;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BuildingStreams
{
	//Streams from values
	
	public static void main(String[] args)
	{
		Stream<String> of = Stream.of("Java8","Lambdas","In","Action");
		
		//Streams from Arrays
		
		int[] numbers = {1,2,3,4,5};
		
		int sum = Arrays.stream(numbers).sum();
		
		// V V Imp
		//streams from files (find number of unique words in the file)
		long uniqueWords = 0;
		
		try
		{
			Stream<String> lines = Files.lines(Paths.get("data.txt"), Charset.defaultCharset());
			//lines.map(d -> Arrays.stream(d.split(" ")));
			lines.flatMap(d -> Arrays.stream(d.split(" "))).distinct().count();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//streams from functions - stream.iterate  stream.generate
		
		 // Stream.iterate
        Stream.iterate(0, n -> n + 2)
              .limit(10)
              .forEach(System.out::println);

        // fibonnaci with iterate
        Stream.iterate(new int[]{0, 1}, t -> new int[]{t[1],t[0] + t[1]})
              .limit(10)
              .forEach(t -> System.out.println("(" + t[0] + ", " + t[1] + ")"));
        
        // V V Imp
        Stream.iterate(new int[]{0, 1}, t -> new int[]{t[1],t[0] + t[1]})
              .limit(10)
              . map(t -> t[0])  
              .forEach(System.out::println);

        // random stream of doubles with Stream.generate
        Stream.generate(Math::random)
              .limit(10)
              .forEach(System.out::println);
 
        // stream of 1s with Stream.generate
        IntStream.generate(() -> 1)
                 .limit(5)
                 .forEach(System.out::println);

        IntStream.generate(new IntSupplier(){
            public int getAsInt(){
                return 2;
            }
        }).limit(5)
          .forEach(System.out::println);
   

        // just for example. storing the state. should be avoided since it can
        // cause issues when using parallel stream
        IntSupplier fib = new IntSupplier(){
                  private int previous = 0;
                  private int current = 1;
                  public int getAsInt(){
                      int nextValue = this.previous + this.current;
                      this.previous = this.current;
                      this.current = nextValue;
                      return this.previous;
                  }
              };
         IntStream.generate(fib).limit(10).forEach(System.out::println);
         
         
         //fibonacci series
         
         Stream.iterate(new int[]{0, 1},
        		 t -> new int[]{t[1],t[0] + t[1]})
        		 .limit(10)
        		 .map(t -> t[0])
        		 .forEach(System.out::println);
         
         
         //stop the stream based on some condition
         //below works with Java9
//         IntStream.iterate(0, n -> n < 100, n -> n + 4)
//         .forEach(System.out::println);
         
         //take while 
//         IntStream.iterate(0, n -> n + 4)
//         .takeWhile(n -> n < 100)
//         .forEach(System.out::println);
         
         
         Stream.generate(Math::random)
         .limit(5)
         .forEach(System.out::println);
	}
	
	
}
