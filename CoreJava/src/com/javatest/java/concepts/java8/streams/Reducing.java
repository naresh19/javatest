package com.javatest.java.concepts.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Reducing
{

	public static void main(String[] args)
	{

		List<Integer> numbers = Arrays.asList(1,2,3,4,5);
		
		//0 is a initial value
		
		int sum = numbers.stream().reduce(0, (a,b)-> a+ b);
		
		int sum1 = numbers.stream().reduce(0, Integer::sum);
		
		//without initial value
		
		Optional<Integer> reduce = numbers.stream().reduce(Integer::sum);
		
		int max = numbers.stream().reduce(0, Integer::max);
		int min = numbers.stream().reduce(0, Integer::min);
		
	}

}
