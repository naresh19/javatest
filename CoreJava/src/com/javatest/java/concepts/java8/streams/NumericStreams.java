package com.javatest.java.concepts.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.javatest.java.concepts.java8.streams.beans.Dish;

public class NumericStreams
{
	
	
	public static void main(String[] args)
	{
		List<Dish> menu = Dish.menu;
		
		// get the total calories of all the dishes
		Integer reduce = menu.stream().map(Dish ::  getCalories).reduce(0,Integer :: sum);
		
		// Integer Stream
		IntStream intStream = menu.stream().mapToInt(Dish ::  getCalories);
		int sum = menu.stream().mapToInt(Dish ::  getCalories).sum();
		
		//convert back to stream of objects
		Stream<Integer> boxed = menu.stream().mapToInt(Dish ::  getCalories).boxed();
		
		//optional int
		OptionalInt max = menu.stream().mapToInt(Dish ::  getCalories).max();
		
		//numeric ranges
		IntStream filter = IntStream.rangeClosed(1, 100).filter(n -> n%2==0);
		
		
	}
}
