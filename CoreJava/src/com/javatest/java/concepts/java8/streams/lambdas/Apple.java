package com.javatest.java.concepts.java8.streams.lambdas;

public class Apple {

	private Integer weight;
	
	private String color;
	
	public Apple() {
		// TODO Auto-generated constructor stub
	}
	
	public Apple(Integer weight) {
		this.weight = weight;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	
	
	
}
