FactoryMethod, ReturnType, UsedTo


toList 				->List<T>
toSet				->Set<T>
toCollection		->Collection<T>
counting			->Long
summingInt			->Integer
averagingInt		->Double
summarizingInt		->IntSummaryStatistics
joining				->String
maxBy				->Optional<T>
minBy				->Optional<T>
reducing			->The type produced by reducing fn
collectingAndThen	->The type produced by transformed fn
groupingBy			->Map<K, List<T>>
partitioningBy		->Map<Boolean,List<T>>