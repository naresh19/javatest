package com.javatest.java.concepts.java8.streams.lambdas;

import static java.util.Comparator.comparing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambaExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Apple> inventory = new ArrayList<Apple>();
		inventory.add(new Apple(1));
		inventory.add(new Apple(3));
		inventory.add(new Apple(4));
		inventory.add(new Apple(2));

		// our aim to sort the list using below simple code
		inventory.sort(comparing(Apple::getWeight));

		// step1:-
		inventory.sort(new AppleComparator());

		// step2:-
		inventory.sort(new Comparator<Apple>() {
			public int compare(Apple a1, Apple a2) {
				return a1.getWeight().compareTo(a2.getWeight());
			}
		});

		// step3:-
		inventory.sort((Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight()));

		// step4:-
		inventory.sort((a1, a2) -> a1.getWeight().compareTo(a2.getWeight()));

		// step5:-
		// use of Comparator.comparing
		// Accepts a function that extracts a Comparable sort key from a type T, and
		// returns a Comparator<T> that compares by that sort key.

		Comparator<Apple> c = Comparator.comparing((Apple a) -> a.getWeight());

		// step6:-
		inventory.sort(comparing(apple -> apple.getWeight()));

		// step7:-
		inventory.sort(comparing(Apple::getWeight));

		// composing predicates and functions.

		Predicate<Apple> redApple = a -> a.getColor().equals("Red");
		Predicate<Apple> notRedApple = redApple.negate();

		Predicate<Apple> redAndHeavyApple = redApple.and(apple -> apple.getWeight() > 150);

		Predicate<Apple> redAndHeavyAppleOrGreen = redApple.and(apple -> apple.getWeight() > 150)
				.or(apple -> "Green".equals(apple.getColor()));

		Function<Integer, Integer> f = x -> x + 1;
		Function<Integer, Integer> g = x -> x * 2;
		Function<Integer, Integer> h = f.andThen(g);
		int result = h.apply(1);
		
		
		Function<String, String> addHeader = Letter::addHeader;
		Function<String, String> transformationPipeline
		= addHeader.andThen(Letter::checkSpelling)
		.andThen(Letter::addFooter);
		transformationPipeline.apply("ha ha");

	}

	public static class AppleComparator implements Comparator<Apple> {
		public int compare(Apple a1, Apple a2) {
			return a1.getWeight().compareTo(a2.getWeight());
		}
	}

	public static class Letter {
		public static String addHeader(String text) {
			return "From Raoul, Mario and Alan: " + text;
		}

		public static String addFooter(String text) {
			return text + " Kind regards";
		}

		public static String checkSpelling(String text) {
			return text.replaceAll("labda", "lambda");
		}
	}

}
