package com.javatest.java.concepts.java8.streams;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

public class Mapping
{

	public static void main(String[] args)
	{

		 // map
        List<String> words = Arrays.asList("Hello", "World");
        List<Integer> wordLengths = words.stream()
                                         .map(String::length)
                                         .collect(toList());
        System.out.println(wordLengths);
        
        // flatMap
        // return a list of all unique characters for list of words
        words.stream()
        		 .map(w -> w.split(""))
                 .flatMap(Arrays::stream) 
                 // here if you use just map , return type will be Stream<Stream<String>>
                 // instead if you use flatMap, return type will be Stream<String>
                 .distinct()
                 .forEach(System.out::println);

        // flatMap
        // return a list of all unique characters for list of words
        words.stream()
                 .flatMap((String line) -> Arrays.stream(line.split("")))
                 .distinct()
                 .forEach(System.out::println);
        
        
       
        
        //doubts--
        System.out.println("Doubts.....");
        words.stream()
		 .map((String line) -> Arrays.stream(line.split(""))).forEach(System.out::println);;
        System.out.println("Doubts.....");

        // flatMap
        // given a list of numbers, how would you return all pairs of numbers?
        // you can represent a pair as an array with 2 numbers
        // input -> {1,2,3}
        // o/p -> {[1,3],[1,4],[2,3],[2,4],[3,3],[3,4]}
        List<Integer> numbers1 = Arrays.asList(1,2,3,4,5);
        List<Integer> numbers2 = Arrays.asList(6,7,8);
        List<int[]> pairs =
                        numbers1.stream()
                                .flatMap((Integer i) -> numbers2.stream()
                                                       .map((Integer j) -> new int[]{i, j})
                                 )
                                .filter(pair -> (pair[0] + pair[1]) % 3 == 0)
                                .collect(toList());
        pairs.forEach(pair -> System.out.println("(" + pair[0] + ", " + pair[1] + ")"));

	}

}
