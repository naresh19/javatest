package com.javatest.java.concepts.java8.streams;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.javatest.java.concepts.java8.streams.beans.Dish;

public class FindingAndMatching
{

	public static void main(String[] args)
	{

		List<Dish> menu = Dish.menu;
		
		 // map
        List<String> words = Arrays.asList("Hello", "World");
        List<Integer> wordLengths = words.stream()
                                         .map(String::length)
                                         .collect(toList());
        System.out.println(wordLengths);
        
        // checking to see if a predicate matches at least one element
        if(menu.stream().anyMatch(d -> d.isVegetarian()))
        {
        	System.out.println("");
        }
        
        boolean allMatch = menu.stream().allMatch(d -> d.isVegetarian());
        boolean noneMatch = menu.stream().noneMatch(d -> d.isVegetarian());
        
        //finding an element
        Optional<Dish> findAny = menu.stream().filter(Dish::isVegetarian).findAny();
        
       menu.stream().filter(Dish::isVegetarian).findAny().ifPresent(d -> System.out.println(d.getName()));;
        
       //finding the first element
       Optional<Dish> findFirst = menu.stream().filter(Dish::isVegetarian).findFirst();

	}

}
