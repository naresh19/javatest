package com.javatest.java.concepts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcRevision
{

    public static void main(String[] args)
        throws SQLException, ClassNotFoundException
    {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1526:TESTID", "root", "root");
        PreparedStatement preStatement = conn.prepareStatement("select distinct item from Order where order_id=?");

        preStatement.setString(0, "123456"); // this will throw
                                             // "java.sql.SQLException: Invalid column index"
                                             // because "0" is not valid colum
                                             // index

        ResultSet result = preStatement.executeQuery();

        while (result.next()) {
            System.out.println("Item: " + result.getString(2)); // this will
                                                                // also throw
                                                                // "java.sql.SQLException: Invalid column index"
                                                                // because
                                                                // resultset has
                                                                // only one
                                                                // column

        }

    }
}
