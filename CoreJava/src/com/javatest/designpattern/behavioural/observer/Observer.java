package com.javatest.designpattern.behavioural.observer;

public interface Observer {

	public void update();

	public void setSubject(Subject subject);
}
