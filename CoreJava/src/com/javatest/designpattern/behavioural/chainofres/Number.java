package com.javatest.designpattern.behavioural.chainofres;

public class Number {
	private int number;

	public Number(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

}
