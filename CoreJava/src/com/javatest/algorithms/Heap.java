package com.javatest.algorithms;

public class Heap
{

	private Integer[] heapData;

	private int heapSize = -1;

	public Heap(int maxSize)
	{
		heapData = new Integer[maxSize];
	}

	public void add(Integer value)
	{
		if (heapSize == heapData.length - 1)
		{
			throw new RuntimeException("Out Of Heap Error");
		}
		heapSize++;
		heapData[heapSize] = value;
		siftUp(heapSize);
	}

	public int getLeftChildIndex(int nodeIndex)
	{
		return (2 * nodeIndex) + 1;
	}

	public int getRightChildIndex(int nodeIndex)
	{
		return (2 * nodeIndex) + 2;
	}

	public int getParentIndex(int nodeIndex)
	{
		return (nodeIndex - 1) / 2;
	}

	public void siftUp(int nodeIndex)
	{
		if (nodeIndex != 0)
		{
			Integer currentNodeValue = heapData[nodeIndex];
			Integer parentIndex = getParentIndex(nodeIndex);
			Integer parentNodeValue = heapData[parentIndex];

			if (parentNodeValue > currentNodeValue)
			{
				// swap
				heapData[nodeIndex] = parentNodeValue;
				heapData[parentIndex] = currentNodeValue;
				siftUp(parentIndex);
			}
		}
	}

	public Integer[] getHeapData()
	{
		return heapData;
	}

	public int deleteItem()
	{
		if (heapSize == -1)
		{
			throw new RuntimeException("Heap Underflow");
		}
		int result = heapData[0];

		int lastElement = heapData[heapSize];
		heapSize--;
		heapData[0] = lastElement;
		siftDown(0);

		return result;
	}

	private void siftDown(int nodeIndex)
	{
		int minIndex = 0;
		int leftChildIndex = getLeftChildIndex(nodeIndex);
		int rightChildIndex = getRightChildIndex(nodeIndex);

		// now check if the nodeIndex has how many childs (left / both)
		if (rightChildIndex > heapSize)
		{
			if (leftChildIndex > heapSize)
			{
				// no childs. stop recursing
				return;
			}
			else
			{
				// it has left child
				minIndex = leftChildIndex;
			}
		}
		else
		{
			// it has both the childs
			if (heapData[rightChildIndex] >= heapData[leftChildIndex])
			{
				minIndex = leftChildIndex;
			}
			else
			{
				minIndex = rightChildIndex;
			}
		}

		if (heapData[minIndex] < heapData[nodeIndex])
		{
			int tmp = heapData[nodeIndex];
			heapData[nodeIndex] = heapData[minIndex];
			heapData[minIndex] = tmp;
			siftDown(minIndex);
		}

	}
}
