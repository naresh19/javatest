package com.javatest.algorithms;

public interface SortingAlgorithm
{

	public Integer[] sort(Integer[] arrayToSort);
}
