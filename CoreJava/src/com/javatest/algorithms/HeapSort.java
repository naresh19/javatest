package com.javatest.algorithms;

import java.util.Arrays;

public class HeapSort implements SortingAlgorithm
{

	public Integer[] sort(Integer[] arrayToSort)
	{
	    //Algorithm
	    
	    //construct a min heap . lowest element will be the top level root
	    //now delete each element .. it will be in ascending order
	    
		Heap heap = new Heap(arrayToSort.length);

		for (int i = 0; i < arrayToSort.length; i++)
		{
			heap.add(arrayToSort[i]);
		}

		System.out.println("After Adding Elements: Current Heap:"
				+ Arrays.asList(heap.getHeapData()));

		Integer[] sortedArray = new Integer[arrayToSort.length];
		for (int i = 0; i < arrayToSort.length; i++)
		{
			sortedArray[i] = heap.deleteItem();
		}

		return sortedArray;

	}

}
