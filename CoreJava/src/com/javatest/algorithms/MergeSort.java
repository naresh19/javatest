package com.javatest.algorithms;

public class MergeSort implements SortingAlgorithm
{

	public Integer[] sort(Integer[] arrayToSort)
	{
		mergeSort(arrayToSort, 0, arrayToSort.length-1);
		return arrayToSort;
	}

	private void mergeSort(Integer[] arrayToSort, int low, int high)
	{
		if (low < high)
		{
			int mid = (low + high) / 2;

			mergeSort(arrayToSort, low, mid);
			mergeSort(arrayToSort, mid + 1, high);

			merge(arrayToSort, low, mid, high);
		}

		
	}

	private void merge(Integer[] arrayToSort, int low, int mid, int high)
	{
		Integer[] scratchArray = new Integer[high - low + 1];

		int i, j, m, n, k = 0;
		i = low;
		m = mid;
		j = mid + 1;
		n = high;

		while (i <= m && j <= n)
		{
			if (arrayToSort[i] < arrayToSort[j])
			{
				scratchArray[k] = arrayToSort[i];
				i++;
				k++;
			}
			else
			{
				scratchArray[k] = arrayToSort[j];
				j++;
				k++;
			}
		}

		if (i <= m)
		{
			for (int l = i; l <= m; l++)
			{
				scratchArray[k] = arrayToSort[l];
				k++;
			}
		}

		if (j <= n)
		{
			for (int l = j; l <= n; l++)
			{
				scratchArray[k] = arrayToSort[l];
				k++;
			}
		}

		// copy the temp array to original array

		for (int h = 0; h < scratchArray.length; h++)
		{
			arrayToSort[h + low] = scratchArray[h];
		}
	}

}
