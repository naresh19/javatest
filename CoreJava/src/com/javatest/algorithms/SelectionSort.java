package com.javatest.algorithms;

public class SelectionSort implements SortingAlgorithm
{

	// 0 5 1 7 2
	public Integer[] sort(Integer[] arrayToSort)
	{
		int minIndex = 0;
		for (int i = 0; i < arrayToSort.length-1; i++)
		{
			minIndex = i;
			for (int j = i + 1; j < arrayToSort.length; j++)
			{
				if (arrayToSort[j] < arrayToSort[minIndex])
				{
					minIndex = j;
				}
			}

			int tmp = arrayToSort[i];
			arrayToSort[i] = arrayToSort[minIndex];
			arrayToSort[minIndex] = tmp;
		}
		return arrayToSort;
	}

}
