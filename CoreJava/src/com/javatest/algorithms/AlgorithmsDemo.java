package com.javatest.algorithms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * @author naresh
 * O(n2)
 */
public class AlgorithmsDemo
{

	private static Map<Integer, SortingAlgorithm> algorithms = new HashMap<Integer, SortingAlgorithm>(
			4);

	static
	{
		algorithms.put(1, new BubbleSort());
		algorithms.put(2, new QuickSort());
		algorithms.put(3, new MergeSort());
		algorithms.put(4, new HeapSort());
		algorithms.put(5, new SelectionSort());
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException
	{

		System.out.println("Select the Algorithm...");
		System.out.println("1.Bubble Sort");
		System.out.println("2.Quick Sort");
		System.out.println("3.Merge Sort");
		System.out.println("4.Heap Sort");
		System.out.println("5.Selection Sort");
		System.out.println("6.Insertion Sort");

		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(System.in));
		String inputString = bufferedReader.readLine();

		Integer selectedAlgorithm = Integer.valueOf(inputString);

		System.out.println("Please input numbers:");
		inputString = bufferedReader.readLine();

		StringTokenizer stringTokenizer = new StringTokenizer(inputString, " ");

		List<Integer> values = new ArrayList<Integer>(4);
		while (stringTokenizer.hasMoreElements())
		{
			String value = (String) stringTokenizer.nextElement();
			values.add(Integer.valueOf(value));
		}

		Integer[] inputArray = values.toArray(new Integer[values.size()]);

		SortingAlgorithm sortingAlgorithm = algorithms.get(selectedAlgorithm);
		Integer[] sort = sortingAlgorithm.sort(inputArray);
		System.out.println("Output:" + Arrays.asList(sort));
	}

}
