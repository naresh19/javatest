package com.javatest.algorithms;

/**
 * @author naresh O(n logn)
 * 
 * Divide & Conquer Algorithm
 * 
 * Select a pivot value (say mid element)
 * arrange the array elements such that left has all lesser values than pivot
 * and right side has all greater elements than pivot
 * 
 * now recursively do this algorithms for 2 halfs which is obtained above
 */
public class QuickSort implements SortingAlgorithm
{

	public Integer[] sort(Integer[] arrayToSort)
	{
		quickSort(arrayToSort, 0, arrayToSort.length - 1);

		return arrayToSort;
	}

	private void quickSort(Integer[] arrayToSort, int left, int right)
	{
		int i = left, j = right;

		int pivot = arrayToSort[(i + j) / 2];

		while (i <= j)
		{
			while (arrayToSort[i] < pivot)
			{
				i++;
			}

			while (arrayToSort[j] > pivot)
			{
				j--;
			}

			if (i <= j)
			{
				int tmp = arrayToSort[i];
				arrayToSort[i] = arrayToSort[j];
				arrayToSort[j] = tmp;
				i++;
				j--;
			}

		}

		if (left < j)
		{
			quickSort(arrayToSort, left, j);
		}
		if (i < right)
		{
			quickSort(arrayToSort, i, right);
		}
	}

}
