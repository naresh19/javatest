package com.javatest.algorithms;

public class BubbleSort implements SortingAlgorithm
{

	public Integer[] sort(Integer[] arrayToSort)
	{
		int length = arrayToSort.length;
		for (int i = 0; i < length; i++)
		{
			for (int j = 0; j < length - 1 - i; j++)
			{
				if (arrayToSort[j] > arrayToSort[j + 1])
				{
					int temp = arrayToSort[j];
					arrayToSort[j] = arrayToSort[j + 1];
					arrayToSort[j + 1] = temp;
				}
			}
		}
		return arrayToSort;
	}

}
