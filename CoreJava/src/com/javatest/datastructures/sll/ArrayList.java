package com.javatest.datastructures.sll;

public class ArrayList
{

    private SingleListNode[] arrayData ;
    
    public ArrayList(int size)
    {
        arrayData = new SingleListNode[size];
    }
    
    class SingleListNode
    {
        int data;
        int nextIndex;
        
        public SingleListNode(int data, int nextIndex)
        {
            this.data = data;
            this.nextIndex = nextIndex;
        }
    }
}
