package com.javatest.datastructures.sll;

import java.util.Hashtable;

public class SingleLinkedList
{

    // programming technique for single linked lists
    // try using multiple pointers at different places to solve the problem
    // start with previous = null at the beginning.
    // many sll  problems tend to be resolved with prev = null
    private SingleLinkedListNode head;

    public SingleLinkedList()
    {
        head = null;
    }

    public void addNodeAtLast(int value)
    {
        SingleLinkedListNode newNode = new SingleLinkedListNode(value);
        if (head == null) {
            head = newNode;
        } else {
            SingleLinkedListNode currentNode = head;

            while (currentNode.nextNode != null) {
                currentNode = currentNode.nextNode;
            }

            currentNode.nextNode = newNode;

        }
    }

    public void addNodeAtFirst(int value)
    {
        SingleLinkedListNode newNode = new SingleLinkedListNode(value);

        if (head == null) {
            head = newNode;
        } else {
            newNode.nextNode = head;
            head = newNode;
        }
    }

    public int traverse()
    {
        int sum = 0;

        SingleLinkedListNode node = head;
        while (node != null) {
            sum = sum + node.value;
            node = node.nextNode;
        }
        return sum;
    }

    public String getCurrentList()
    {
        String contents = "";

        SingleLinkedListNode node = head;
        while (node != null) {
            contents = contents + "-" + node.value;
            node = node.nextNode;
        }
        return contents;
    }

    public int findMidElement()
    {
        SingleLinkedListNode oneStepIncrPointer = head;
        SingleLinkedListNode twoStepIncrPointer = head;

        while (twoStepIncrPointer != null) {
            if (null != twoStepIncrPointer.nextNode) {
                twoStepIncrPointer = twoStepIncrPointer.nextNode.nextNode;
            } else {
                twoStepIncrPointer = null;
            }

            if (null == twoStepIncrPointer) {
                break;
            }

            oneStepIncrPointer = oneStepIncrPointer.nextNode;
        }
        return oneStepIncrPointer.value;
    }

    public SingleLinkedListNode findNthNodeFromLast(SingleLinkedListNode head)
    {
        /*
         * we just use 2 pointers � call them pntr1 and pntr2. Let�s say that we
         * advance pntr2 by N-1 nodes. After that, every time we iterate through
         * the linked list, both pntr1 and pntr2 will be advanced to point to
         * the very next node in the linked list. This means that there will
         * always be a distance of exactly n-1 nodes between pntr1 and pntr2.
         * So, when pntr2 is equal to null, this means that pntr2 has reached
         * the very end of the list. And, most importantly, it means that pntr1
         * will be pointing at the nth to last node! So, we will have our
         * answer!
         * 
         * Node pntr1, pntr2 = head;
         * for  (int  i  =  0;  i  <  n  - 1;  ++i)  {  

               pntr2 = pntr2.nextNode;

               }



    while(pntr2.nextNode != null)
    {
          pntr1 = pntr1.nextNode;
          pntr2 = pntr2.nextNode;
    }

    return pntr1;
         */
        return null;
    }

    public void reverse()
    {
        reverse(head);
    }

    private void reverse(SingleLinkedListNode node)
    {
        SingleLinkedListNode currentNode = node;

        SingleLinkedListNode prev = null;

        while (null != currentNode) {
            SingleLinkedListNode temp = currentNode.nextNode;
            currentNode.nextNode = prev;
            prev = currentNode;
            currentNode = temp;
        }

        head = prev;
    }

    public boolean findLoop()
    {
        /*
         * bool hasLoop(Node *head) { Node *slow = head, *fast = head; while
         * (slow && fast && fast->next) { slow = slow->next; fast =
         * fast->next->next; if(slow == fast) return true; } return false; }
         */
        return true;
    }

    public void deleteDups()
    {
        SingleLinkedListNode n = head;
        Hashtable table = new Hashtable();
        SingleLinkedListNode previous = null;
        while (n != null) {
            if (table.containsKey(n.value))
                previous.nextNode = n.nextNode;
            else {
                table.put(n.value, true);
                previous = n;
            }
            n = n.nextNode;
        }
    }

}
