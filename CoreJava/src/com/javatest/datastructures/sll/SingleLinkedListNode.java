package com.javatest.datastructures.sll;

public class SingleLinkedListNode
{

	public  int value;

	public SingleLinkedListNode nextNode;

	public SingleLinkedListNode(int val)
	{
		this.value = val;
		nextNode = null;
	}

}
