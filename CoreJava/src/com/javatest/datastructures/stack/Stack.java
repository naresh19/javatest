package com.javatest.datastructures.stack;

public class Stack
{

	private int top;

	private int[] data;

	public Stack(int size)
	{
		data = new int[size];
		top = -1;
	}

	public void push(int val)
	{
		if(top == data.length-1)
		{
			throw new RuntimeException("Stack Overflow");
		}
		
		top++;
		data[top] =  val;
	}
	
	public int pop()
	{
		if(top == -1)
		{
			throw new RuntimeException("Stack Underflow");
		}
		int val =  data[top];
		top--;
		return val;
	}
	
	public int peek()
	{
		return data[top];
	}
}
