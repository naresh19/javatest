package com.javatest.datastructures.queue;


// front , rear
// elements added at rear
// element deleted at front
// this is circular queue
// FIFO
public class CircularQueue
{

    private int[] que;
    private int rear;
    private int front;
    private int maxSize;
    
    public CircularQueue(int s)
    {
        maxSize = s;
        que = new int[maxSize];
        front = rear = -1;
    }
    
    public void enqueue(int data) throws Exception
    {
        if(isFull())
        {
            throw new Exception("Q Full");
        }
        else
        {
            rear = (rear+1)%maxSize;
            que[rear] = data;
            if(front == -1)
            {
                front = rear;
            }
        }
    }
    
    public int dequeue() throws Exception
    {
        if(isEmpty())
        {
            throw new Exception("Q empty");
        }
        else
        {
             int data = que[front];
             if(front == rear)
             {
                 front = rear = -1;
             }
             else
             {
                 front= (front +1) % maxSize;
             }
             
             return data;
        }
        
    }
    
    private boolean isEmpty()
    {
        return front == -1;
    }

    public boolean isFull()
    {
        return ((rear+1)%maxSize == front);
    }
    
   
}
