package com.javatest.datastructures.queue;

public class LinkedQueue
{

    class Node
    {
        Node next;
        Object data;
        
        public Node(Object data)
        {
            this.data = data;
        }
    }
    
    private Node front;
    private Node rear;
    int count;
    
    public void insert(Object item)
    {
        Node p = new Node(item);
        
        // check if que is empty
        if(front  == null)
        {
            front = rear = p;
        }
        else
        {
            rear.next = p;
            rear = p;
        }
        
        count++;
        
    }
    
    public Object remove()
    {
        //check if q is empty
        if(front == null)
        {
            System.out.println("Q is empty");
            return 0;
        }
        
        Object item = front.data;
        front = front.next;
        count--;
        return item;
    }
    
    public Object peek()
    {
        return front.data;
    }
    
}
