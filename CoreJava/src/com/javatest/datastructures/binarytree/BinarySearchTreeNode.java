package com.javatest.datastructures.binarytree;

public class BinarySearchTreeNode
{

	public BinarySearchTreeNode(int value)
	{
		this.value = value;
	}

	public BinarySearchTreeNode left;
	public BinarySearchTreeNode right;
	
	public int value;
	
	
}
