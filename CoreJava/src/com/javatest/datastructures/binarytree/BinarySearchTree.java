package com.javatest.datastructures.binarytree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public class BinarySearchTree
{

    private BinarySearchTreeNode root;
    public BinarySearchTree()
    {
        root = new BinarySearchTreeNode(10);
    }
    
    
    //building a tree from array
    //initially we pass index as 0
    public BinarySearchTreeNode buildTree(int a[], int index)
    {
        BinarySearchTreeNode p  = null;
        
        if(a[index] != 999999)
        {
            p = new BinarySearchTreeNode(a[index]);
            p.left = buildTree(a, (2*index +1));
            p.right = buildTree(a, (2*index+2));
        }
        
        return p;
    }
    
	// finding depth of binary tree
	public int heightOfBinaryTree(BinarySearchTreeNode node)
	{
		if (node == null)
		{
			return 0;
		}
		else
		{
			return 1 + Math.max(heightOfBinaryTree(node.left),
					heightOfBinaryTree(node.right));
		}
	}

	public void insert(BinarySearchTreeNode node, int value)
	{
		if (value < node.value)
		{
			if (node.left != null)
			{
				insert(node.left, value);
			}
			else
			{
				System.out.println("  Inserted " + value + " to left of node "
						+ node.value);
				node.left = new BinarySearchTreeNode(value);
			}
		}
		else if (value > node.value)
		{
			if (node.right != null)
			{
				insert(node.right, value);
			}
			else
			{
				System.out.println("  Inserted " + value + " to right of node "
						+ node.value);
				node.right = new BinarySearchTreeNode(value);
			}
		}
	}
	
	public BinarySearchTreeNode searchInBST(BinarySearchTreeNode root,int key)
	{
	    BinarySearchTreeNode p = root;
	     while(p !=null)
	     {
	         if(key == p.value)
	         {
	             return p;
	         }
	         else if(key < p.value)
	         {
	             p = p.left;
	         }
	         else
	         {
	             p = p.right;
	         }
	     }
	     return null;
	}
	

	// this is nothing but depth first traversal.
	// preorder,postorder are also variations of DFS while preorder is the 
	//perfect generalization of DFS
	public void printInOrder(BinarySearchTreeNode node)
	{
		if (node != null)
		{
			printInOrder(node.left);
			System.out.println("  Traversed " + node.value);
			printInOrder(node.right);
		}
	}
	
	//level order traversal
	// this is nothing  but Breadth first traversal
	// for depth first search u can use stack
	public void levelOrder(BinarySearchTreeNode p)
	{
	 
	    //A Linked list itself can be used as a Q
	    // by using methods - addLast, removeFirst
	    LinkedList<BinarySearchTreeNode> q = new LinkedList<BinarySearchTreeNode>();
	    q.addLast(p);
	    
	    while(!q.isEmpty())
	    {
	        p = q.removeFirst();
	        System.out.println(p.value + " ");
	        if(p.left != null)
	        {
	            q.addLast(p.left);
	        }
	        if(p.right != null)
	        {
	            q.addLast(p.right);
	        }
	    }
	}
	
	//this search is for normal binary trees.
	//for binary search tree we need to search based value is < or > root
	public boolean search(BinarySearchTreeNode p,int data)
	{
	    Queue<BinarySearchTreeNode> q = new LinkedBlockingQueue<BinarySearchTreeNode>();
        q.offer(p);
        
        while(!q.isEmpty())
        {
            p = q.remove();
            
            if(data == p.value)
            {
                return true;
            }
            if(p.left != null)
            {
                q.add(p.left);
            }
            if(p.right != null)
            {
                q.add(p.right);
            }
        }
        return false;
	}
	
	// Tree Image
	public boolean checkIfLeftSubTreeIsImageOfRightSubtree(BinarySearchTreeNode root)
	{
	    /*
	     *      1
              /   \
             2     2
            / \   / \
           4   3 3   4
	     * */
	    /*
	     *Use level order traversal and store all the rows in say linked list,
	     *reverse each row and check if it is palindrome or not 
	     * */
	    return true;
	}

	// convert a tree into level linked lists
	public ArrayList<LinkedList<BinarySearchTreeNode>> findLevelLinkList(BinarySearchTreeNode root)
	{
		int level = 0;
		ArrayList<LinkedList<BinarySearchTreeNode>> result = new ArrayList<LinkedList<BinarySearchTreeNode>>();
		LinkedList<BinarySearchTreeNode> list = new LinkedList<BinarySearchTreeNode>();
		list.add(root);
		result.add(level, list);
		while (true)
		{
			list = new LinkedList<BinarySearchTreeNode>();
			for (int i = 0; i < result.get(level).size(); i++)
			{
				BinarySearchTreeNode n = (BinarySearchTreeNode) result.get(level).get(i);
				if (n != null)
				{
					if (n.left != null)
						list.add(n.left);
					if (n.right != null)
						list.add(n.right);
				}
			}
			if (list.size() > 0)
			{
				result.add(level + 1, list);
			}
			else
			{
				break;
			}
			level++;
		}
		return result;
	}
	
	//Difference Between Sums of Odd and Even Levels in Binary Trees
	public int diffBetween(BinarySearchTreeNode pRootNode)
	{
	   if (pRootNode == null) 
	      return 0;

	   int lvalue = diffBetween(pRootNode.left);
	   int rvalue = diffBetween(pRootNode.right);

	   int result = pRootNode.value - (lvalue + rvalue);
	   return result;
	}
	
	//BQ - use queue
    public void BreadthFirstSearch(BinarySearchTreeNode root)
    {
        Queue<BinarySearchTreeNode> que = new LinkedBlockingDeque<BinarySearchTreeNode>();
        que.offer(root);

        while (!que.isEmpty()) {
            BinarySearchTreeNode node = que.poll();
            System.out.println(node.value + " ");
            if (null != node.left) {
                que.offer(node.left);
            }
            if (null != node.right) {
                que.offer(node.right);
            }

        }

    }
	
	//DS- use stack
    public void DepthFirstSearch(BinarySearchTreeNode root)
    {
        Stack<BinarySearchTreeNode> stack = new Stack<BinarySearchTreeNode>();
        stack.push(root);

        while (!stack.isEmpty()) {
            BinarySearchTreeNode node = stack.pop();
            System.out.println(node.value + " ");
            if (null != node.left) {
                stack.push(node.left);
            }
            if (null != node.right) {
                stack.push(node.right);
            }

        }
    }
}
