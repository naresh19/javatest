package com.javatest.xml;


public class XMLDifferences
{

	public Conflicts conflicts;
	
	public LeftSideAdditionalEntries leftSideAdditionalEntries;
	
	public RightSideAdditionalEntries rightSideAdditionalEntires;
	
	

	public LeftSideAdditionalEntries getLeftSideAdditionalEntries()
	{
		return leftSideAdditionalEntries;
	}

	public void setLeftSideAdditionalEntries(
			LeftSideAdditionalEntries leftSideAdditionalEntries)
	{
		this.leftSideAdditionalEntries = leftSideAdditionalEntries;
	}

	public RightSideAdditionalEntries getRightSideAdditionalEntires()
	{
		return rightSideAdditionalEntires;
	}

	public void setRightSideAdditionalEntires(
			RightSideAdditionalEntries rightSideAdditionalEntires)
	{
		this.rightSideAdditionalEntires = rightSideAdditionalEntires;
	}

	public Conflicts getConflicts()
	{
		return conflicts;
	}

	public void setConflicts(Conflicts conflicts)
	{
		this.conflicts = conflicts;
	}

	
	
}
