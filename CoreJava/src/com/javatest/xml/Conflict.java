package com.javatest.xml;

public class Conflict
{

	public String id;
	public String employeeName1;
	public String employeeName2;
	public String deptId1;
	public String deptId2;
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getEmployeeName1()
	{
		return employeeName1;
	}
	public void setEmployeeName1(String employeeName1)
	{
		this.employeeName1 = employeeName1;
	}
	public String getEmployeeName2()
	{
		return employeeName2;
	}
	public void setEmployeeName2(String employeeName2)
	{
		this.employeeName2 = employeeName2;
	}
	public String getDeptId1()
	{
		return deptId1;
	}
	public void setDeptId1(String deptId1)
	{
		this.deptId1 = deptId1;
	}
	public String getDeptId2()
	{
		return deptId2;
	}
	public void setDeptId2(String deptId2)
	{
		this.deptId2 = deptId2;
	}
	
}
