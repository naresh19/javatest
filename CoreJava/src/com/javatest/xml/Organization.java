package com.javatest.xml;

import java.util.List;

public class Organization
{

	private List<Employee> employees;

	public List<Employee> getEmployees()
	{
		return employees;
	}

	public void setEmployees(List<Employee> employees)
	{
		this.employees = employees;
	}
	
	
}
