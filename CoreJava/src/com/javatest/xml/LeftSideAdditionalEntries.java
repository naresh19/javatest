package com.javatest.xml;

import java.util.List;

public class LeftSideAdditionalEntries
{

	public List<Employee> employees;

	public List<Employee> getEmployees()
	{
		return employees;
	}

	public void setEmployees(List<Employee> employees)
	{
		this.employees = employees;
	}
}
