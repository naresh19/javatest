package com.javatest.xml;

import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

public class XMLDifferenceFinder
{

	@SuppressWarnings("unchecked")
	public static void main(String args[])
	{

		Mapping mapping = new Mapping();

		try
		{
			// 1. Load the mapping information from the file
			mapping.loadMapping("mapping.xml");

			// 2. Unmarshal the data
			Unmarshaller unmar = new Unmarshaller(mapping);
			Organization organization1 = (Organization) unmar
					.unmarshal(new InputSource(
							new FileReader("SampleFile1.xml")));
			Organization organization2 = (Organization) unmar
					.unmarshal(new InputSource(
							new FileReader("SampleFile2.xml")));

			List<Employee> employees1 = organization1.getEmployees();
			List<Employee> employees2 = organization2.getEmployees();

			List<Conflict> conflicts = new ArrayList<Conflict>(2);

			List<Employee> leftSideAdditionalEntries = new ArrayList<Employee>(
					2);
			List<Employee> rightSideAdditionalEntries = new ArrayList<Employee>(
					2);

			leftSideAdditionalEntries.addAll(CollectionUtils.subtract(
					employees1, employees2));
			rightSideAdditionalEntries.addAll(CollectionUtils.subtract(
					employees2, employees1));

			for (Employee employee : employees1)
			{
				if (employees2.contains(employee))
				{
					int indexOf = employees2.indexOf(employee);
					Employee employee2 = employees2.get(indexOf);

					if (employee.getName().equals(employee2.getName())
							&& employee.getDeptId().equals(
									employee2.getDeptId()))
					{
						continue;
					}
					else
					{
						Conflict conflict = new Conflict();
						conflict.setId(employee.getId());
						conflict.setEmployeeName1(employee.getName());
						conflict.setDeptId1(employee.getDeptId());
						conflict.setEmployeeName2(employee2.getName());
						conflict.setDeptId2(employee2.getDeptId());
						conflicts.add(conflict);
					}
				}
			}

			XMLDifferences differences = new XMLDifferences();
			Conflicts conflicts2 = new Conflicts();
			conflicts2.setConflicts(conflicts);
			differences.setConflicts(conflicts2);
			LeftSideAdditionalEntries left = new LeftSideAdditionalEntries();
			left.setEmployees(leftSideAdditionalEntries);

			RightSideAdditionalEntries right = new RightSideAdditionalEntries();
			right.setEmployees(rightSideAdditionalEntries);

			differences.setLeftSideAdditionalEntries(left);
			differences.setRightSideAdditionalEntires(right);

			System.out.println(differences);

			Mapping resultmapping = new Mapping();
			resultmapping.loadMapping("differencesmapping.xml");
			Marshaller marshaller = new Marshaller(new OutputStreamWriter(
					System.out));
			marshaller.setMapping(resultmapping);
			marshaller.marshal(differences);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}
	}
}
