package com.javatest.xml;

public class Employee
{

	public String id;

	public String name;

	public String deptId;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDeptId()
	{
		return deptId;
	}

	public void setDeptId(String deptId)
	{
		this.deptId = deptId;
	}

	public boolean equals(Object obj)
	{
		if (null == obj || this.getClass() != obj.getClass())
		{
			return false;
		}
		if (this == obj)
		{
			return true;
		}
		Employee employee = (Employee) obj;
		return employee.getId().equals(this.id);
	}

	public int hashCode()
	{
		return id.hashCode();
	}

}
