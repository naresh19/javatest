package com.javatest.xml;

import java.util.List;

public class Conflicts
{
	public List<Conflict> conflicts;

	public List<Conflict> getConflicts()
	{
		return conflicts;
	}

	public void setConflicts(List<Conflict> conflicts)
	{
		this.conflicts = conflicts;
	}
}
