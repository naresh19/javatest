package com.microservices.demo.currencyexchangeservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservices.demo.currencyexchangeservice.beans.ExchangeValue;

public interface CurrencyExchangeRepository extends JpaRepository<ExchangeValue, Long> {

	ExchangeValue findByFromAndTo(String from,String to);
}
