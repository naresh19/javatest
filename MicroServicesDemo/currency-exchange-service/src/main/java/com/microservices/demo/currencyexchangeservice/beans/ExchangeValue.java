package com.microservices.demo.currencyexchangeservice.beans;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ExchangeValue {

	@Id
	private Long id;
	
	@Column(name="from_currency")
	
	private String from;
	
	@Column(name="to_currency")
	private String to;
	
	private BigDecimal exchangeMultiplier;
	
	private int port;
	

	public ExchangeValue() {
	}

	public ExchangeValue(Long id, String from, String to, BigDecimal exchangeMultiplier) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.exchangeMultiplier = exchangeMultiplier;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public BigDecimal getExchangeMultiplier() {
		return exchangeMultiplier;
	}

	public void setExchangeMultiplier(BigDecimal exchangeMultiplier) {
		this.exchangeMultiplier = exchangeMultiplier;
	}
	
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	
}
