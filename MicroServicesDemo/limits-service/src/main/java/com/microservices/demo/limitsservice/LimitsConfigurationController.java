package com.microservices.demo.limitsservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.demo.limitsservice.beans.LimitsConfiguration;

@RestController
public class LimitsConfigurationController {
	
	@Autowired
	private LimitsData limitsData;

	@GetMapping("/limits")
	public LimitsConfiguration getLimitsFromConfig()
	{
		return new LimitsConfiguration(limitsData.getMinimum(), limitsData.getMaximum());
	}
}
