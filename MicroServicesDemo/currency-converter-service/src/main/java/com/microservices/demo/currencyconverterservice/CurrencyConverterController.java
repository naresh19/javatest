package com.microservices.demo.currencyconverterservice;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.demo.currencyconverterservice.bean.CurrencyConverterBean;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;

@RestController
@RequestMapping("/currency-converter")
public class CurrencyConverterController {
	
	@Autowired
	private CurrencyConverterServiceProxy ccsp;
	
	@GetMapping("/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConverterBean getConvertionValue(@PathVariable("from") String from, @PathVariable("to") String to,
			@PathVariable("quantity") BigDecimal quantity) {
		
		CircuitBreaker circuitBreaker = CircuiteBreakerHelper.getCircuitBreaker("retrieveExchangeValue");		CurrencyConverterBean ccb = ccsp.retrieveExchangeValue(from, to);
		return new CurrencyConverterBean(from, to, ccb.getExchangeMultiplier(), quantity,
				quantity.multiply(ccb.getExchangeMultiplier()), ccb.getPort());
	}

}
