package com.microservices.demo.currencyconverterservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.microservices.demo.currencyconverterservice.bean.CurrencyConverterBean;

@FeignClient(name="currency-exchange-service")
public interface CurrencyConverterServiceProxy

{
	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyConverterBean retrieveExchangeValue(@PathVariable("from") String from,@PathVariable("to") String to);
	
}
