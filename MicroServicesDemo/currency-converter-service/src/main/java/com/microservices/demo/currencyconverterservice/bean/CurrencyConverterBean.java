package com.microservices.demo.currencyconverterservice.bean;

import java.math.BigDecimal;

public class CurrencyConverterBean {

	private String from;
	private String to;
	private BigDecimal exchangeMultiplier;
	private BigDecimal quantity;
	private BigDecimal totalValue;
	private int port;
	
	public CurrencyConverterBean() {
		// TODO Auto-generated constructor stub
	}

	public CurrencyConverterBean(String from, String to, BigDecimal exchangeMultiplier, BigDecimal quantity,
			BigDecimal totalValue, int port) {
		super();
		this.from = from;
		this.to = to;
		this.exchangeMultiplier = exchangeMultiplier;
		this.quantity = quantity;
		this.totalValue = totalValue;
		this.port = port;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public BigDecimal getExchangeMultiplier() {
		return exchangeMultiplier;
	}

	public void setExchangeMultiplier(BigDecimal exchangeMultiplier) {
		this.exchangeMultiplier = exchangeMultiplier;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	
	
}
