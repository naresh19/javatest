package com.microservices.demo.currencyconverterservice;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;

public class CircuiteBreakerHelper {

	public static CircuitBreaker getCircuitBreaker(String name) {
		CircuitBreakerConfig config = CircuitBreakerConfig.custom()
				// Percentage of failures to start short-circuit
				.failureRateThreshold(20)
				// Min number of call attempts
				.ringBufferSizeInClosedState(5).build();
		CircuitBreakerRegistry registry = CircuitBreakerRegistry.of(config);
		return  registry.circuitBreaker(name);
	}
}
