package com.innovationfollowers.grvredressal.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.innovationfollowers.grvredressal.model.Departments;

@Repository
public class DepartmentRepositoryImpl implements DepartmentRepository 
{

	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public Long addDepartment(Departments dept)
	{
		entityManager.persist(dept);
		return dept.getId();
	}

	@Override
	public void updateDepartment(Departments dept) 
	{
		entityManager.merge(dept);
	}

	@Override
	public void deleteDepartment(Departments dept) 
	{
		entityManager.remove(dept);
	}

	@Override
	public Departments findById(Long id) 
	{
		return entityManager.find(Departments.class, id);
	}

    @Override
    public List<Departments> getAllDepartments() 
    {
      List<Departments> depts = entityManager.createNativeQuery("select * from DEPARTMENTS", Departments.class).getResultList();
      return depts;
    }

}
