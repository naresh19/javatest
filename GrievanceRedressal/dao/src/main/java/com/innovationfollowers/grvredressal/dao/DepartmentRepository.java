package com.innovationfollowers.grvredressal.dao;

import java.util.List;
import com.innovationfollowers.grvredressal.model.Departments;

public interface DepartmentRepository 
{

	Long addDepartment(Departments dept);
	
	void updateDepartment(Departments dept);
	
	void deleteDepartment(Departments dept);
	
	Departments findById(Long id);
	
	List<Departments> getAllDepartments();
}
