package com.innovationfollowers.grvredressal.service;

import java.util.List;
import com.innovationfollowers.grvredressal.model.Departments;

public interface DepartmentService 
{

	Long addDepartment(Departments dept);

	void updateDepartment(Departments dept);

	void deleteDepartment(Long deptId);

	Departments findById(Long id);
	
	List<Departments> getAllDepartments();
	
}
