package com.innovationfollowers.grvredressal.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.innovationfollowers.grvredressal.dao.DepartmentRepository;
import com.innovationfollowers.grvredressal.model.Departments;

@Transactional
@Service
public class DepartmentServiceImpl implements DepartmentService {
	
	@Autowired
	private DepartmentRepository deptRepo;

	@Override
	public Long addDepartment(Departments dept) {
		return deptRepo.addDepartment(dept);
	}

	@Override
	public void updateDepartment(Departments dept) {
		deptRepo.updateDepartment(dept);
	}

	@Override
	public void deleteDepartment(Long deptId) {
		Departments department = this.findById(deptId);
        deptRepo.deleteDepartment(department);
	}

	@Override
	public Departments findById(Long id) {
		return deptRepo.findById(id);
	}

    @Override
    public List<Departments> getAllDepartments() {
      return deptRepo.getAllDepartments();
    }

}
