CREATE SCHEMA `grievanceredressal` ;

CREATE TABLE `grievanceredressal`.`DEPARTMENTS` (
  `DEPT_ID` INT NOT NULL AUTO_INCREMENT,
  `DEPT_NAME` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`DEPT_ID`));


CREATE TABLE `grievanceredressal`.`OFFICIALS` (
  `OFFICIAL_ID` INT NOT NULL AUTO_INCREMENT,
  `OFFICIAL_NAME` VARCHAR(45) NOT NULL,
  `DEPT_ID` INT NOT NULL,
  PRIMARY KEY (`OFFICIAL_ID`),
  INDEX `fk_OFFICIALS_DEPT_ID_idx` (`DEPT_ID` ASC),
  CONSTRAINT `fk_OFFICIALS_DEPT_ID`
    FOREIGN KEY (`DEPT_ID`)
    REFERENCES `grievanceredressal`.`DEPARTMENTS` (`DEPT_ID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
    
CREATE TABLE `grievanceredressal`.`GRIEVANCES` (
  `GRIEV_ID` INT NOT NULL AUTO_INCREMENT,
  `GRIEV_DESC` varchar(1000) NOT NULL,
  `APPLICANT_DETAILS` varchar(500) NOT NULL,
  `SUBMITTED_DATE` datetime NOT NULL,
  `DEPT_ID` INT NOT NULL,
  `STATUS` varchar(45) NOT NULL,
  PRIMARY KEY (`GRIEV_ID`),
  KEY `fk_GRIEVANCES_DEPT_ID_idx` (`DEPT_ID`),
  CONSTRAINT `fk_GRIEVANCES_DEPT_ID` FOREIGN KEY (`DEPT_ID`) REFERENCES `DEPARTMENTS` (`DEPT_ID`) ON DELETE CASCADE ON UPDATE NO ACTION
);


CREATE TABLE `grievanceredressal`.`GRIEVANCE_ASSIGNMENTS` (
  `ASSIGNMT_ID` INT NOT NULL AUTO_INCREMENT,
  `GRIEV_ID` INT NOT NULL,
  `STATUS` VARCHAR(45) NOT NULL,
  `OFFICIAL_ID` INT NOT NULL,
  `TASK_COMMENTS` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`ASSIGNMT_ID`),
  INDEX `fk_GRIEVANCE_ASSIGNMENTS_OFFICIAL_ID_idx` (`OFFICIAL_ID` ASC),
  INDEX `fk_GRIEVANCE_ASSIGNMENTS_GRIEV_ID_idx` (`GRIEV_ID` ASC),
  CONSTRAINT `fk_GRIEVANCE_ASSIGNMENTS_OFFICIAL_ID`
    FOREIGN KEY (`OFFICIAL_ID`)
    REFERENCES `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_GRIEVANCE_ASSIGNMENTS_GRIEV_ID`
    FOREIGN KEY (`GRIEV_ID`)
    REFERENCES `grievanceredressal`.`GRIEVANCES` (`GRIEV_ID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);


INSERT INTO `grievanceredressal`.`DEPARTMENTS` (`DEPT_ID`, `DEPT_NAME`) VALUES ('1', 'DEPT OF TELECOMMUNICATIONS');
INSERT INTO `grievanceredressal`.`DEPARTMENTS` (`DEPT_ID`, `DEPT_NAME`) VALUES ('2', 'HOME MINISTRY');
INSERT INTO `grievanceredressal`.`DEPARTMENTS` (`DEPT_ID`, `DEPT_NAME`) VALUES ('3', 'EXTERNAL AFFAIRS');
INSERT INTO `grievanceredressal`.`DEPARTMENTS` (`DEPT_ID`, `DEPT_NAME`) VALUES ('4', 'TOURISM');

 
    
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('1', 'NARESH', '1');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('2', 'MANOJ', '1');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('3', 'RAMESH', '2');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('4', 'SHWETHA', '2');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('5', 'INDIRA', '3');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('6', 'KRISHNA', '3');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('7', 'PRIYANKA', '4');
INSERT INTO `grievanceredressal`.`OFFICIALS` (`OFFICIAL_ID`, `OFFICIAL_NAME`, `DEPT_ID`) VALUES ('8', 'SHYAM', '4');


INSERT INTO `grievanceredressal`.`GRIEVANCES` (`GRIEV_ID`, `GRIEV_DESC`, `APPLICANT_DETAILS`, `SUBMITTED_DATE`, `DEPT_ID`, `STATUS`) VALUES ('1', 'broadband not working, no response from airtel', 'kishan', '2019-12-19 22:00:00', '1', 'ASSIGNED');


INSERT INTO `grievanceredressal`.`GRIEVANCE_ASSIGNMENTS` (`ASSIGNMT_ID`, `GRIEV_ID`, `STATUS`, `OFFICIAL_ID`, `TASK_COMMENTS`) VALUES ('1', '1', 'NOT STARTED', '1', 'NOT STARTED');






