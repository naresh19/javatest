package com.innovationfollowers.grvredressal.api;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.innovationfollowers.grvredressal.model.Departments;
import com.innovationfollowers.grvredressal.model.dto.DepartmentDTO;
import com.innovationfollowers.grvredressal.model.mapper.DepartmentMapper;
import com.innovationfollowers.grvredressal.service.DepartmentService;

@Component
@Path("/departmentService")
public class DepartmentController 
{

	@Autowired
	private DepartmentService deptSrvc;
	
	
	@POST
	@Path("addDepartment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addDepartment(DepartmentDTO deptDto) {

		Departments dept = DepartmentMapper.INSTANCE.dtoToEntity(deptDto);
		deptSrvc.addDepartment(dept);
		return Response.ok(dept).build();
	}
	
	@GET
    @Path("getAllDepartments")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getAllDepartments()
	{
	  List<DepartmentDTO> depts = new ArrayList<DepartmentDTO>();
	  List<Departments> allDepartments = deptSrvc.getAllDepartments();
	  for (Departments departments : allDepartments) {
        depts.add(DepartmentMapper.INSTANCE.entityToDTO(departments));
      }
	  return Response.ok(depts).build();
	}
	
}
