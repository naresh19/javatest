package com.innovationfollowers.grvredressal.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "officials")
public class Officials
{

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "OFFICIAL_ID")
  private Long id;
  
  @Column(name = "OFFICIAL_NAME")
  private String name;
  
  @Column(name = "DEPT_ID")
  private Long deptId;
  
  public Officials() {
    // TODO Auto-generated constructor stub
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getDeptId() {
    return deptId;
  }

  public void setDeptId(Long deptId) {
    this.deptId = deptId;
  }
  
  
}
