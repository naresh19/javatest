package com.innovationfollowers.grvredressal.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GRIEVANCES")
public class Grievances 
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "GRIEV_ID")
	private Long id;
	
	@Column(name = "GRIEV_DESC")
	private String desc;
	
	@Column(name = "APPLICANT_DETAILS")
	private String applicantDetails;
	
	@Column(name = "SUBMITTED_DATE")
	private Date submittedDate;
	
	@Column(name = "DEPT_ID")
	private Long deptId;
	
	private String status;
	
	public Grievances() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getApplicantDetails() {
		return applicantDetails;
	}

	public void setApplicantDetails(String applicantDetails) {
		this.applicantDetails = applicantDetails;
	}

	public Date getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
