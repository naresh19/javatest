package com.innovationfollowers.grvredressal.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.innovationfollowers.grvredressal.model.Departments;
import com.innovationfollowers.grvredressal.model.dto.DepartmentDTO;

@Mapper(componentModel = "spring")
public interface DepartmentMapper {

	DepartmentMapper INSTANCE = Mappers.getMapper(DepartmentMapper.class);

	DepartmentDTO entityToDTO(Departments source);

	Departments dtoToEntity(DepartmentDTO destination);
}