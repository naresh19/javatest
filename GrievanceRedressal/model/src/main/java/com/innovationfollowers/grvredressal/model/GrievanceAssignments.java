package com.innovationfollowers.grvredressal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GRIEVANCE_ASSIGNMENTS")
public class GrievanceAssignments 
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ASSIGNMT_ID")
	private Long id;
	
	@Column(name = "GRIEV_ID")
	private Long grieveId;

	private String status;
	
	@Column(name = "OFFICIAL_ID")
	private Long officialId;
	
	@Column(name = "TASK_COMMENTS")
	private String taskComments;
	
	public GrievanceAssignments() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGrieveId() {
		return grieveId;
	}

	public void setGrieveId(Long grieveId) {
		this.grieveId = grieveId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getOfficialId() {
		return officialId;
	}

	public void setOfficialId(Long officialId) {
		this.officialId = officialId;
	}

	public String getTaskComments() {
		return taskComments;
	}

	public void setTaskComments(String taskComments) {
		this.taskComments = taskComments;
	}
	
	
}
