package com.springboot.springbootdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldRestController {

  @GetMapping(path="/hello-world")
  public String helloWorld()
  {
    return "Welcome to Spring Boot";
  }
  
  
  @GetMapping(path="/say-hello-world")
  public HelloWorldBean sayHelloWorld()
  {
    return new HelloWorldBean("Hey,there you go!");
  }
}
